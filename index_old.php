<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="js/js.js"></script>


<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
    <div class="outerpage">
    	<div class="nav">
    	    <a href="#" class="menu"><h1>< Menu</h1></a>
    	    <div class="midt">
    	    	<a href="#" class="back"><h1><</h1></a>
					<h1 class="header">Salg i Estate Media</h1>
					<a href="#" class="forth"><h1>></h1></a>
    	    </div>
    	    
		</div>
    	<div class="page">
    	    <div class="top">
    	    	<div class="maned">
				<h1>Juli</h1>
									
				
				<div class="kolonne beskrivelse">
					<div class="kolonnedel">
						
					</div>
					<div class="kolonnedel">
						<p>Bud. annon.</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Rekl.</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Spons.</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Sum budget</p>
					</div>
					<div class="kolonnedel">
						<p>Sum realis.</p>
					</div>
					<div class="kolonnedel">
						<p>Difference</p>
					</div>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 1</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p class="<?php if($i == 8){echo "diff";}?> "><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 2</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "-10.000");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p class="<?php if($i== 8){echo "diff negativ";}?>"><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 3</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p class="<?php if($i== 8){echo "diff";}?>"><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
					
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Samlet</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p class="<?php if($i== 8){echo "diff";}?>"><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				
			</div>
    	    	<div class="maned">
				<h1>August</h1>
				<div class="kolonne beskrivelse">
					<div class="kolonnedel">
						
					</div>
					<div class="kolonnedel">
						<p>Bud. annoncer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Online</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Sponsorer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Sum budget</p>
					</div>
					<div class="kolonnedel">
						<p>Sum realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Difference</p>
					</div>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 1</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 2</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Sælger 3</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
					
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Samlet</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				
			</div>
    	    	<div class="maned">
				<h1>September</h1>
				<div class="kolonne beskrivelse">
					<div class="kolonnedel">
						
					</div>
					<div class="kolonnedel">
						<p>Bud. annoncer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Online</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Sponsorer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Sum budget</p>
					</div>
					<div class="kolonnedel">
						<p>Sum realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Difference</p>
					</div>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Michael</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Betina</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Nikolaj</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
					
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Samlet</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				
			</div>
    	    	<div class="maned">
				<h1>Oktober</h1>
				<div class="kolonne beskrivelse">
					<div class="kolonnedel">
						
					</div>
					<div class="kolonnedel">
						<p>Bud. annoncer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Online</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Sponsorer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Sum budget</p>
					</div>
					<div class="kolonnedel">
						<p>Sum realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Difference</p>
					</div>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Michael</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Betina</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Nikolaj</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
					
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Samlet</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				
			</div>
    	    	<div class="maned">
				<h1>November</h1>
				<div class="kolonne beskrivelse">
					<div class="kolonnedel">
						
					</div>
					<div class="kolonnedel">
						<p>Bud. annoncer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Online</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Bud. Sponsorer</p>
					</div>
					<div class="kolonnedel">
						<p>Realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Sum budget</p>
					</div>
					<div class="kolonnedel">
						<p>Sum realiseret</p>
					</div>
					<div class="kolonnedel">
						<p>Difference</p>
					</div>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Michael</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Betina</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Nikolaj</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
					
				</div>
				<div class="kolonne person">
					<div class="kolonnedel">
						<p>Samlet</p>
					</div>
					<?php 
					$data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					
					for($i = 0; $i < 9; $i++){ ?>
						<div class="kolonnedel">
							<p><?php echo $data[$i]; ?></p>
						</div>
					<?php }	?>
				</div>
				
			</div>
				<div class="maned">
					<h1>December</h1>
					<div class="kolonne beskrivelse">
					    <div class="kolonnedel">
					    	
					    </div>
					    <div class="kolonnedel">
					    	<p>Bud. annoncer</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Realiseret</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Bud. Online</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Realiseret</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Bud. Sponsorer</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Realiseret</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Sum budget</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Sum realiseret</p>
					    </div>
					    <div class="kolonnedel">
					    	<p>Difference</p>
					    </div>
					</div>
					<div class="kolonne person">
					    <div class="kolonnedel">
					    	<p>Michael</p>
					    </div>
					    <?php 
					    $data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					    
					    for($i = 0; $i < 9; $i++){ ?>
					    	<div class="kolonnedel">
					    		<p><?php echo $data[$i]; ?></p>
					    	</div>
					    <?php }	?>
					</div>
					<div class="kolonne person">
					    <div class="kolonnedel">
					    	<p>Betina</p>
					    </div>
					    <?php 
					    $data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					    
					    for($i = 0; $i < 9; $i++){ ?>
					    	<div class="kolonnedel">
					    		<p><?php echo $data[$i]; ?></p>
					    	</div>
					    <?php }	?>
					</div>
					<div class="kolonne person">
					    <div class="kolonnedel">
					    	<p>Nikolaj</p>
					    </div>
					    <?php 
					    $data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					    
					    for($i = 0; $i < 9; $i++){ ?>
					    	<div class="kolonnedel">
					    		<p><?php echo $data[$i]; ?></p>
					    	</div>
					    <?php }	?>
					    
					</div>
					<div class="kolonne person">
					    <div class="kolonnedel">
					    	<p>Samlet</p>
					    </div>
					    <?php 
					    $data = array("35.000", "37.478", "0", "0", "0", "0", "35.000", "37.478", "2.478");
					    
					    for($i = 0; $i < 9; $i++){ ?>
					    	<div class="kolonnedel">
					    		<p><?php echo $data[$i]; ?></p>
					    	</div>
					    <?php }	?>
					</div>
			</div>
    	    	
    	    </div>
    	    <div class="bottom">
    	    	<?php 
    	    	
    	    	$navne = array('Michael', 'Katja', 'Adnan', 'Samlet');
    	    	
    	    	$taller = 1; 
    	    	
    	    	for($i=1;$i<7;$i++){?>
    	    	
    	    	<div class="maned">
				<div class="rakke">
					<h1>Michael</h1>   
					<canvas id="canvas<?php echo $taller++; ?>" height="210" width="420"></canvas>
				</div>
				<div class="rakke">
					<h1>Katja</h1>
					<canvas id="canvas<?php echo $taller++; ?>" height="77" width="420"></canvas>
				</div>
				<div class="rakke">
					<h1>Adnan</h1>
					<canvas id="canvas<?php echo $taller++; ?>" height="77" width="420"></canvas>
				</div>
				<div class="rakke">
					<h1>Samlet</h1>
					<canvas id="canvas<?php echo $taller++; ?>" height="77" width="420"></canvas>
				</div>
				
			</div>
    	    	<?php } ?>
    	
    	</div>
	</div>
</body>
</html>