<?php include("login_kontrol.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js/js.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage forside">
		<img class="logo" src="media/andet/estatemedia.png" title="Estate Media" alt="Estate Media" />
		<h1 class="head">Bookingsystem</h1>
		<p class="logud"><a href="https://estateviden.dk/admin">Konferencer</a></p>
		<p class="logud"><a href="logud.php">Log ud</a></p>
		<div class="linkbokse">
			<div class="stats linkboks">
				<a href="stats.php"><h1>Salgsbudget</h1></a>
			</div>
			<div class="stats linkboks">
				<a href="magasiner.php"><h1>Annonceoversigt</h1></a>
			</div>
			<div class="online linkboks">
				<div class="datadel">
					<a href="online_pr_uge.php"><h1>Online annoncer<br />per uge</h1></a>
					<a href="online.php"><h1>Ledigt onlinesalg</h1></a>
				</div>
			</div>
			<div class="data linkboks">
				<div class="datadel">
					<a href="data/"><h1>Ordrer</h1></a>
				</div>
			</div>
			<?php if($admin == "1"){?>
			<div class="admin linkboks">
				<h1 class="rubrik">Administration</h1>
				<a href="stats_samlet.php"><h1>Samlet salgsbudget</h1></a>
				<a href="ar_til_dato.php"><h1>År til dato</h1></a>
				<a href="data/budget_liste.php"><h1>Rediger budget</h1></a>
			</div>
			<?php } ?>
		</div>
	
	</div>


</body>
</html>