<?php

include("../database.php");

$dato = array();
$realiseret = array();
$budget = array();


$ar = date("Y");

for($i = 1; $i < 13; $i++){
	if($i < 10){
		$maned = "0".$i;
		} 
	else {
		$maned = $i;
		}
	
	
	$months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
	$dato[] = $months_short[$i-1];
	
	
	$sqldato = $ar . "-" . $maned . "-15";
	$resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato'");
	$data = mysql_fetch_array($resultat);
	if($data["belob"] != ""){
		$realiseret[] = $data["belob"];	
	}
	else {
		$realiseret[] = "0";
	}
	

	$resultat = mysql_query("SELECT SUM(annoncer) + SUM(reklame) + SUM(sponsor) as tal FROM salg_budget WHERE maned = '$sqldato'");
	$data = mysql_fetch_array($resultat);
	if($data["tal"] != ""){
		$budget[] = $data["tal"];	
	}
	else {
		$budget[] = "0";
	}
	}

?>

var lineChartData = {
    labels : [<?php foreach($dato as $date){echo "\"$date\", ";} ?>],
    datasets : [
    	{
    		fillColor : "rgba(220,220,220,0)",
    		strokeColor : "rgba(23, 148, 0 ,0.5)",
    		pointColor : "rgba(220,220,220,1)",
    		pointStrokeColor : "#fff",
    		data : [<?php foreach($budget as $budgetet){echo "$budgetet, ";} ?>]
    		},
    	{
    		fillColor : "rgba(151,187,205,0)",
    		strokeColor : "rgba(0, 100, 204 ,0.5)",
    		pointColor : "rgba(151,187,205,1)",
    		pointStrokeColor : "#fff",
    		data : [<?php foreach($realiseret as $real){echo "$real, ";} ?>]
    		}
    	]
    
    }

new Chart(document.getElementById("samlet_omsatning").getContext("2d")).Line(lineChartData, {scaleOverride: false});