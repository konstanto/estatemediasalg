<?php

include("../database.php");

$dato = array();
$salger1 = array();
$salger2 = array();
$salger3 = array();

$ar = date("Y");

for($i = 1; $i < 13; $i++){
	if($i < 10){
		$maned = "0".$i;
		} 
	else {
		$maned = $i;
		}
	
	
	$months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
	$dato[] = $months_short[$i-1];
	
	
	$sqldato = $ar . "-" . $maned . "-15";
	$resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '1'");
	$data = mysql_fetch_array($resultat);
	if($data["belob"] != ""){
		$salger1[] = $data["belob"];	
	}
	else {
		$salger1[] = "0";
	}
	
	
	$resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '2'");
	$data = mysql_fetch_array($resultat);
	if($data["belob"] != ""){
		$salger2[] = $data["belob"];	
	}
	else {
		$salger2[] = "0";
	}
	
	$resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '3'");
	$data = mysql_fetch_array($resultat);
	if($data["belob"] != ""){
		$salger3[] = $data["belob"];	
	}
	else {
		$salger3[] = "0";
	}
	
	}

?>


var lineChartData = {
    labels : [<?php foreach($dato as $date){echo "\"$date\", ";} ?>],
    datasets : [
    	{
    		fillColor : "rgba(220,220,220,0)",
    		strokeColor : "rgba(23, 148, 0 ,0.5)",
    		pointColor : "rgba(220,220,220,1)",
    		pointStrokeColor : "#fff",
    		data : [<?php foreach($salger2 as $salger){echo "$salger, ";} ?>]
    		},
    	{
    		fillColor : "rgba(151,187,205,0)",
    		strokeColor : "rgba(0, 100, 204 ,0.5)",
    		pointColor : "rgba(151,187,205,1)",
    		pointStrokeColor : "#fff",
    		data : [<?php foreach($salger3 as $salger){echo "$salger, ";} ?>]
    		}
    	,
    	{
    		fillColor : "rgba(151,187,205,0)",
    		strokeColor : "rgba(255, 193, 0, 0.5)",
    		pointColor : "rgba(151,187,205,1)",
    		pointStrokeColor : "#fff",
    		data : [<?php foreach($salger1 as $salger){echo "$salger, ";} ?>]
    		}
    	]
    
    }

new Chart(document.getElementById("realiseret").getContext("2d")).Line(lineChartData, {scaleOverride: false});