<?php include("../login_kontrol.php"); include("../database.php");
	session_start();
	$loginbruger = $_SESSION["brugerid"];
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="Chart.js"></script>

<!--[if lte IE 8]>
		<script src="js/excanvas.../js"></script>
	<![endif]-->

<script type="text/javascript" src="../js/js.js"></script>


<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body id="hej" class="ar_til_dato print">
	<div class="page">
		<div class="content">
			<div class="realiseret del">
				<h1>Realiseret salg pr. måned pr. sælgerkode</h1>
				
				<div class="graf">
					<canvas id="realiseret" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_ar.php");
							}
						
						 ?>
					</script>
					
					<div class="beskrivelse">
					<p class="et">Michael</p>
					<p class="to">Katja</p>
					<p class="tre">Adnan</p>
				</div>
					
					
				</div>
			</div>
			<div class="samlet_omsatning del">
				<h1>Samlet omsætning realiseret vs. budget pr måned</h1>
				
				<div class="graf">
					<canvas id="samlet_omsatning" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/samlet_omsatning_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/samlet_omsatning_ar.php");
							}
						
						 ?>
					</script>
					<div class="beskrivelse">
						<p class="et">Budget</p>
						<p class="to">Realiseret</p>
					</div>
					
					
					
				</div>
			</div>
			<div class="realiseret_procent del">
				<h1>Realiseret i % af samlet salg pr. sælgerkode</h1>
				
				<div class="graf">
					<canvas id="realiseret_procent" height="400" width="440"></canvas>
					
					
					<script>

						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_procent_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_procent_ar.php");
							}
						
						 ?>
	
	</script>
				<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_procent_ar_procenter.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_procent_ar_procenter.php");
							}
						
						 ?>
				
				
				
				<div class="beskrivelse">
					<p class="et">Michael: <br /><?php echo round($procent2); ?>%</p>
					<p class="to">Katja: <br /><?php echo round($procent3); ?>%</p>
					<p class="tre">Adnan: <br /><?php echo round($procent1); ?>%</p>
				</div>	
					
					
					
				</div>
			</div>
			<div class="samlet_omsatning_akumuleret del">
				<h1>Samlet omsætning realiseret vs. budget akkumuleret over perioden</h1>
				
				<div class="graf">
					<canvas id="samlet_omsatning_akumuleret" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_ar.php");
							}
						
						 ?>
					</script>
					<div class="beskrivelse">
					<p class="et">Budget</p>
					<p class="to">Realiseret</p>
				</div>
					
					
					
				</div>
			</div>
		</div>
		
	</div>
</body>
</html>