<?php include("../database.php");
	session_start();
	$loginbruger = $_SESSION["brugerid"];
	
$e = $_GET["e"];	
	

if($e == "ar"){	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="Chart.js"></script>

<!--[if lte IE 8]>
		<script src="js/excanvas.../js"></script>
	<![endif]-->

<script type="text/javascript" src="../js/js.js"></script>


<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body id="hej" class="ar_til_dato print">
	<div class="page">
		<div class="content">
			<div class="realiseret del">
				<h1>Realiseret salg pr. måned pr. sælgerkode</h1>
				
				<div class="graf">
					<canvas id="realiseret" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_ar.php");
							}
						
						 ?>
					</script>
					
					<div class="beskrivelse">
					<p class="et">Michael</p>
					<p class="to">Katja</p>
					<p class="tre">Adnan</p>
				</div>
					
					
				</div>
			</div>
			<div class="samlet_omsatning del">
				<h1>Samlet omsætning realiseret vs. budget pr måned</h1>
				
				<div class="graf">
					<canvas id="samlet_omsatning" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/samlet_omsatning_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/samlet_omsatning_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/samlet_omsatning_ar.php");
							}
						
						 ?>
					</script>
					<div class="beskrivelse">
						<p class="et">Budget</p>
						<p class="to">Realiseret</p>
					</div>
					
					
					
				</div>
			</div>
			<div class="realiseret_procent del">
				<h1>Realiseret i % af samlet salg pr. sælgerkode</h1>
				
				<div class="graf">
					<canvas id="realiseret_procent" height="400" width="440"></canvas>
					
					
					<script>

						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_procent_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_procent_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_procent_ar.php");
							}
						
						 ?>
	
	</script>
				<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/realiseret_procent_ar_procenter.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/realiseret_procent_kvartal_procenter.php");
							}
							else {
								include("../ar_til_dato_data/realiseret_procent_ar_procenter.php");
							}
						
						 ?>
				
				
				
				<div class="beskrivelse">
					<p class="et">Michael: <br /><?php echo round($procent2); ?>%</p>
					<p class="to">Katja: <br /><?php echo round($procent3); ?>%</p>
					<p class="tre">Adnan: <br /><?php echo round($procent1); ?>%</p>
				</div>	
					
					
					
				</div>
			</div>
			<div class="samlet_omsatning_akumuleret del">
				<h1>Samlet omsætning realiseret vs. budget akkumuleret over perioden</h1>
				
				<div class="graf">
					<canvas id="samlet_omsatning_akumuleret" height="400" width="440"></canvas>
					
					
					<script type="text/javascript">
						<?php 
							if($_GET["type"] == "ar"){
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_ar.php");
							}
							else if($_GET["type"] == "kvart1"){
								$kvartal = 1;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart2"){
								$kvartal = 2;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart3"){
								$kvartal = 3;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else if($_GET["type"] == "kvart4"){
								$kvartal = 4;
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_kvartal.php");
							}
							else {
								include("../ar_til_dato_data/samlet_omsatning_akumuleret_ar.php");
							}
						
						 ?>
					</script>
					<div class="beskrivelse">
					<p class="et">Budget</p>
					<p class="to">Realiseret</p>
				</div>
					
					
					
				</div>
			</div>
		</div>
		
	</div>
</body>
</html>
<?php
}
?>









<?php
if($e == "stats"){	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="Chart.js"></script>

<!--[if lte IE 8]>
		<script src="../js/excanvas.js"></script>
	<![endif]-->

<script type="text/javascript" src="../js/js.js"></script>


<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body class="statssamlet print_stats">
	<div class="page">
		
		<div class="content">
			
			<?php
								
			$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								
			for($i = 0; $i < 6; $i++){
				
				// Vi kører igennem 6 måneder
				if(isset($_GET["dato"])){
					$date = strtotime("20" . $_GET["dato"] . "-15 + ". $i." months");
				}
				else {
					$date = strtotime("today + ". $i." months");	
				}
				
				$datotilsql = date("Y-m", $date) . "-15";
				?>
			<div class="maned">
				<h1 class="manedsnavn"><?php echo $months[date("n", $date)-1]; ?></h1>
				<div class="top">
					<div class="topdel beskrivelse">
						<div class="beskrivelsesdel felt"></div>
						<div class="beskrivelsesdel felt">
						    <p>Bud. annoncer</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Realiseret</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Bud. Online</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Realiseret</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Bud. Sponsor</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Realiseret</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Sum budget</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Sum realiseret</p>
						</div>
						<div class="beskrivelsesdel felt">
						    <p>Difference</p>
						</div>	
					</div>
					
					<?php
					
					//Laver så man ikke behøver at skrive kolonnerne for alle sælgerne enkeltvis. De skal dog stå i bestemt rækkefølge, så derfor bliver de konstrueret ud fra nedentående array
					
					$salgere = array(2, 3, 1);
					$sellerNames = array("Adnan", "Michael", "Katja");
					$counter = 1;
					
					foreach($salgere as $salgerid){
						?>
						
						<div class="topdel salger">
						<?php 
						$resultat = mysql_query("SELECT * FROM salg_budget WHERE maned = '$datotilsql' AND salgerid = '$salgerid'");
						$salger_budget = mysql_fetch_array($resultat);
						
						$sum_realiseret = 0;
						
						?>
						
						<div class="salgsdel navn felt">
							<h2><?php echo $sellerNames[$salgerid - 1]; ?></h2>
						</div>
						
						
						
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["annoncer"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type != 2 AND type!= 3");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						
						
						
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["reklame"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type = 2");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						
						
						
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["sponsor"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type = 3");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						<div class="salgsdel">
							<p><?php echo number_format($sum_budget = (int)$salger_budget["annoncer"]*1 + (int)$salger_budget["reklame"]*1 + (int)$salger_budget["sponsor"]*1, 0, ',', '.'); ?></p>
						</div>
						<div class="salgsdel">
							<p><?php echo number_format($sum_realiseret, 0, ',', '.'); ?></p>
						</div>
						<div class="salgsdel diff <?php if(($diff = $sum_realiseret - $sum_budget) < 0) {echo "negativ";} ?>">
							<p><?php echo number_format(($diff), 0, ',', '.'); ?></p>
						</div>
					</div>
						
						
						
						
						<?php
						
						$counter++;
					}
					?>
					
					
					
					<div class="topdel salger">
						<?php
						
						
						//Det samlede resultat
						
						$resultat = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE maned = '$datotilsql'");
						$salger_budget = mysql_fetch_array($resultat);
						
						$sum_realiseret = 0;
						
						?>
						
						
						<div class="salgsdel navn felt">
							<h2>Samlet</h2>
						</div>
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["annoncebudget"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type != 2 AND type != 3");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["reklamebudget"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type = 2");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						<div class="salgsdel todel">
							<div class="to">
								<p><?php echo number_format($salger_budget["sponsorbudget"], 0, ',', '.'); ?></p>
							</div>
							<div class="to">
								<p><?php 
								
								$resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type = 3");
								$sum_lokal = mysql_fetch_array($resultat);
								
								$sum_realiseret += $sum_lokal["count"];
								
								echo number_format($sum_lokal["count"], 0, ',', '.');
								?></p>
							</div>
						</div>
						<div class="salgsdel">
							<p><?php echo number_format($sum_budget = (int)$salger_budget["annoncebudget"]*1 + (int)$salger_budget["reklamebudget"]*1 + (int)$salger_budget["sponsorbudget"]*1, 0, ',', '.'); ?></p>
						</div>
						<div class="salgsdel">
							<p><?php echo number_format($sum_realiseret, 0, ',', '.'); ?></p>
						</div>
						<div class="salgsdel diff <?php if(($diff = $sum_realiseret - $sum_budget) < 0) {echo "negativ";} ?>">
							<p><?php echo number_format(($diff), 0, ',', '.'); ?></p>
						</div>
					</div>
				
				</div>
				
				
				
				
				
				
				
				<div class="bottom">
					<div class="barchart">
						<h1><?php echo $months[date("n", $date)-1]; ?></h1>
						
						<canvas id="bar<?php echo $i; ?>" height="740" width="150"></canvas>
						<script type="text/javascript">
							<?php
							
								//Vi tester for om der er data for denne måned. Hvis ikke skal den bare udskrive 0
								if($sum_budget != ""){
									$sum_budget = $sum_budget/1000;
									}
								else {
									$sum_budget = "0";	
									}
								
									
								if($sum_realiseret != ""){
									$sum_realiseret = $sum_realiseret/1000;
									}
								else {
									$sum_realiseret = "0";	
									}
								?>
								var barChartData = {
							    		labels : [""],
							    		datasets : [
							    			{
							    				fillColor : "rgba(0, 100, 204 ,0.5)",
							    				strokeColor : "rgba(220,220,220,1)",
							    				data : ['<?php echo $sum_budget; ?>']
							    			},
							    			{
							    				fillColor : "rgba(23, 148, 0 ,0.5)",
							    				strokeColor : "rgba(220,220,220,1)",
							    				data : ['<?php echo $sum_realiseret; ?>']
							    			}
							    		]
							    		
							    	}
							    	
							    	
									<?php $highest = max($sum_budget, $sum_realiseret); ?>
							    new Chart(document.getElementById("bar<?php echo $i; ?>").getContext("2d")).Bar(barChartData, {scaleSteps : 14<?php //echo round($highest/25); ?>, scaleStepWidth : 50});	
						</script>
						
						
						<p class="bud">Bud.</p>
						<p class="rea">Rea.</p>
					</div>
					
					<div class="grafer">
						<div class="graf">
							<div class="graftext">
								<h1>Michael</h1>
								<p class="bud">Sum bud.</p>
								<p class="rea">Sum Rea.</p>
								
							</div>
							<canvas id="canvas<?php echo $i; ?>_1" height="187" width="635"></canvas>
							
							
						</div>
						<div class="graf">
							<div class="graftext">
								<h1>Katja</h1>
								<p class="bud">Sum bud.</p>
								<p class="rea">Sum Rea.</p>
								
							</div>
							
							<canvas id="canvas<?php echo $i; ?>_2" height="187" width="635"></canvas>
							
							
						</div>
						<div class="graf">
							<div class="graftext">
								<h1>Adnan</h1>
								<p class="bud">Sum bud.</p>
								<p class="rea">Sum Rea.</p>
								
							</div>
							
							<canvas id="canvas<?php echo $i; ?>_3" height="187" width="635"></canvas>
							
						</div>
						<div class="graf">
							<div class="graftext">
								<h1>Samlet</h1>
								<p class="bud">Sum bud.</p>
								<p class="rea">Sum Rea.</p>
							</div>
							<canvas id="canvas<?php echo $i; ?>_4" height="187" width="635"></canvas>
						</div>
							
							
						<script type="text/javascript">
						    
						    <?php
						    
						    $salgere = array(2, 3, 1);
						    $counter = 1;
						    
						    foreach($salgere as $salgerid){
						    
						    
						    //Vi laver arrays for alle værdierne i de kommende 6 måneder fra udgangspunkt
						    	$months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
						    	
						    	$maned_navn = array();
						    	$maned_budget = array();
						    	$maned_realiseret = array();
						    	
						    	for($ii = 0; $ii < 6; $ii++){
							    	$date = strtotime("$datotilsql + ". $ii." months");
									$chartdato = date("Y-m", $date) . "-15";
							    	
							    	$maned_navn[] = $months_short[date("n", $date)-1];	
							    	
							    	
							    	$resultat5 = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE salgerid = '$salgerid' AND maned = '$chartdato'");
							    	$budgettet = mysql_fetch_array($resultat5);
							    	
							    	$maned_budget[] = ($budgettet["reklamebudget"] + $budgettet["annoncebudget"] + $budgettet["sponsorbudget"]);
							    	
							    	
							    	$resultat5 = mysql_query("SELECT SUM(ordrebelob) as ordrebelob FROM salg_handler WHERE salgerid = '$salgerid' AND maned = '$chartdato'");
							    	$real = mysql_fetch_array($resultat5);
							    	
							    	if($real["ordrebelob"] != ""){
								    	$maned_realiseret[] = $real["ordrebelob"];
							    	}
							    	else {
								    	$maned_realiseret[] = 0;
							    	}	
						    	}
						    ?>
						    
								var lineChartData = {
								    	labels : ["<?php echo $maned_navn[0]; ?>","<?php echo $maned_navn[1]; ?>","<?php echo $maned_navn[2]; ?>","<?php echo $maned_navn[3]; ?>","<?php echo $maned_navn[4]; ?>","<?php echo $maned_navn[5]; ?>"],
								    	datasets : [
								    		{
								    			fillColor : "rgba(220,220,220,0)",
								    			strokeColor : "rgba(0, 100, 204 ,0.5)",
								    			pointColor : "rgba(220,220,220,1)",
								    			pointStrokeColor : "#fff",
								    			data : [<?php echo $maned_budget[0]; ?>, <?php echo $maned_budget[1]; ?>, <?php echo $maned_budget[2]; ?>, <?php echo $maned_budget[3]; ?>, <?php echo $maned_budget[4]; ?>, <?php echo $maned_budget[5]; ?>]
								    		},
								    		{
								    			fillColor : "rgba(151,187,205,0)",
								    			strokeColor : "rgba(23, 148, 0 ,0.5)",
								    			pointColor : "rgba(151,187,205,1)",
								    			pointStrokeColor : "#fff",
								    			data : [<?php echo $maned_realiseret[0]; ?>, <?php echo $maned_realiseret[1]; ?>, <?php echo $maned_realiseret[2]; ?>, <?php echo $maned_realiseret[3]; ?>, <?php echo $maned_realiseret[4]; ?>, <?php echo $maned_realiseret[5]; ?>]
								    		}
								    	]
								    	
								    }
								    
								    
								    
								var myLine = new Chart(document.getElementById("canvas<?php echo $i; ?>_<?php echo $counter; ?>").getContext("2d")).Line(lineChartData,  {scaleSteps : 10, scaleStepWidth : 30000});
						    <?php
						    	$counter++;
						    }
						    ?>
						    
						    
						    <?php
						    
						    
						    //Vi laver arrays for alle værdierne i de kommende 6 måneder fra udgangspunkt
						    	$months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
						    	
						    	$maned_navn = array();
						    	$maned_budget = array();
						    	$maned_realiseret = array();
						    	
						    	for($ii = 0; $ii < 6; $ii++){
							    	$date = strtotime("$datotilsql + ". $ii." months");
									$chartdato = date("Y-m", $date) . "-15";
							    	
							    	$maned_navn[] = $months_short[date("n", $date)-1];	
							    	
							    	
							    	$resultat5 = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE maned = '$chartdato'");
							    	$budgettet = mysql_fetch_array($resultat5);
							    	
							    	$maned_budget[] = ($budgettet["reklamebudget"] + $budgettet["annoncebudget"] + $budgettet["sponsorbudget"]);
							    	
							    	
							    	$resultat5 = mysql_query("SELECT SUM(ordrebelob) as ordrebelob FROM salg_handler WHERE maned = '$chartdato'");
							    	$real = mysql_fetch_array($resultat5);
							    	
							    	if($real["ordrebelob"] != ""){
								    	$maned_realiseret[] = $real["ordrebelob"];
							    	}
							    	else {
								    	$maned_realiseret[] = 0;
							    	}	
						    	}
						    ?>
						    
								var lineChartData = {
								    	labels : ["<?php echo $maned_navn[0]; ?>","<?php echo $maned_navn[1]; ?>","<?php echo $maned_navn[2]; ?>","<?php echo $maned_navn[3]; ?>","<?php echo $maned_navn[4]; ?>","<?php echo $maned_navn[5]; ?>"],
								    	datasets : [
								    		{
								    			fillColor : "rgba(220,220,220,0)",
								    			strokeColor : "rgba(0, 100, 204 ,0.5)",
								    			pointColor : "rgba(220,220,220,1)",
								    			pointStrokeColor : "#fff",
								    			data : [<?php echo $maned_budget[0]; ?>, <?php echo $maned_budget[1]; ?>, <?php echo $maned_budget[2]; ?>, <?php echo $maned_budget[3]; ?>, <?php echo $maned_budget[4]; ?>, <?php echo $maned_budget[5]; ?>]
								    		},
								    		{
								    			fillColor : "rgba(151,187,205,0)",
								    			strokeColor : "rgba(23, 148, 0 ,0.5)",
								    			pointColor : "rgba(151,187,205,1)",
								    			pointStrokeColor : "#fff",
								    			data : [<?php echo $maned_realiseret[0]; ?>, <?php echo $maned_realiseret[1]; ?>, <?php echo $maned_realiseret[2]; ?>, <?php echo $maned_realiseret[3]; ?>, <?php echo $maned_realiseret[4]; ?>, <?php echo $maned_realiseret[5]; ?>]
								    		}
								    	]
								    	
								    }
								    
								    
								    
								var myLine = new Chart(document.getElementById("canvas<?php echo $i; ?>_4").getContext("2d")).Line(lineChartData, {scaleSteps : 10, scaleStepWidth : 50000});
						    
						</script>
							
							
							
							
							
							
							
						
					</div>
				</div>	
			</div>
			<?php } ?>
			
			
			
			
			
		</div>
	</div>
</body>
</html>
<?php
}
?>