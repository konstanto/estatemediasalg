<?php include("login_kontrol.php"); include("../database.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=8"></meta> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<script src="../js/datepicker/jquery.ui.core.js"></script>
<script src="../js/datepicker/jquery.ui.datepicker.js"></script>
<script src="../js/datepicker/jquery.ui.datepicker-da.js"></script>
<link rel="stylesheet" href="../js/datepicker/jquery.ui.all.css">
<link rel="stylesheet" href="../js/datepicker/demos.css">
<script type="text/javascript">
$(function() {
		$( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
		
	});
</script>




<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder">
		<div class="opretboks">
			<div class="header">
				<a href="../forside.php" class="menu"><h1>Menu</h1></a>
				<h1>Opret ny ordre</h1>
			</div>
			<form method="post" action="send_ny_ordre.php">
				<table>
					<tr>
						<td colspan="2"><p>Kundenavn:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="kunde" /></td>
					</tr>
					
					<tr>
						<td><p>Beløb:</p></td>
						<td><p>Faktureringsmåned:</p></td>
					</tr>
					<tr>
						<td><input type="text" name="belob" /></td>
						<td>
							<select name="maned">
								<?php
								
								$today = date("Y-m", strtotime('today')) . "-15";
								
								$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								for($i = 0; $i < 24; $i++){
									$date = strtotime($today . " + " . $i." months");
									?>
									<option value="<?php echo date("Y-m", $date); ?>-15"><?php echo $months[date("n", $date)-1] . " " . date("Y", $date); ?></option>
									<?php
								}
								?>
								
								
								
							</select>
						</td>
					</tr>
					<tr>
						<td><p>Sælger:</p></td>
						<td><p>Type:</p></td>
					</tr>
					<tr>
						<td>
							<select name="salger">
								<option value="2">Michael</option>
								<option value="3">Katja</option>
								<option value="1">Adnan</option>
							</select>
						</td>
						<td>
							<select class="typevalger" name="type">
								<option value="1">Annonce - print</option>
								<option value="4">Annonce - web</option>
								<option value="5">Brancheguide</option>
								<option value="2">Reklame</option>
								<option value="3">Sponsor</option>
								<option value="6">MoreCard</option>
								<option value="7">Diverse</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2"><p>Ordrenummer:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="ordrenummer" /></td>
					</tr>
					<tr class="magasinrakke">
						<td><p>Magasinnummer:</p></td>
						<td><p>Størrelse:</p></td>
					</tr>
					<tr class="magasinrakke">
						<td>
							<select name="magasinnr">
								<?php
								$resultat = mysql_query("SELECT * FROM salg_magasiner WHERE id = '1'");
								if(!$resultat){
									  die('Could not connect: ' . mysql_error());
									  }
							
								$magasiner = mysql_fetch_array($resultat);
								
								$magasiner = unserialize($magasiner["magasinarray"]);
								
								foreach($magasiner as $magasin){
								?>
									<option value="<?php echo $magasin; ?>">Nr. <?php echo substr($magasin, 2); ?> - 20<?php echo substr($magasin, 0, 2); ?></option>
								<?php
								}
								
								
								?>
								
							</select>
						</td>
						<td>
							<select name="storrelse">
								<option value="0.125">0.125 side</option>
								<option value="0.25">0.25 side</option>
								<option value="0.5">0.5 side</option>
								<option value="1">1 side</option>
								<option value="2">2 side</option>
							</select>
						</td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2"><p>Status:</p></td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2">
							<select name="status" style="width: 101%;">
								<option value="0">-</option>
								<option value="1">Kunde ikke kontaktet</option>
								<option value="2">Kunde kontaktet</option>
								<option value="3">Materiale modtaget</option>
								<option value="4">Materiale godkendt</option>
							</select>
						</td>
					</tr>
					
					<tr>
						<td colspan="2"><p>Note:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="magasinnote"></textarea></td>
					</tr>
					
					<tr>
						<td colspan="2"><input type="submit" value="Opret ordre" />
					</tr>
			
			
				</table>
			</form>
		</div>
	
	</div>


</body>
</html>