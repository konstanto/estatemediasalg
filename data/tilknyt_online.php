<?php include("login_kontrol.php"); include("../database.php"); 
	
	$id = $_GET["ordreid"];
	$maned = $_GET["maned"];
	
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder ">
		<div class="opretboks">
			<div class="header">
				<a href="rediger_ordre.php?maned=<?php echo $maned; ?>&id=<?php echo $id; ?>" class="menu"><h1>Tilbage</h1></a>
				<h1>Tilknyt online annonce</h1>
				<a href="tilknyt_online_flere_uger.php?maned=<?php echo $maned; ?>&id=<?php echo $id; ?>" class="flere_uger"><h1>Multi booking</h1></a>
			</div>
			<form method="post" action="tilknyt_online_send.php">
				<input type="hidden" name="ordreid" value="<?php echo $id; ?>" />
				<input type="hidden" name="oldmaned" value="<?php echo $maned; ?>" />
				<table>
					<tr>
						<td colspan="2"><p>Uge:</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="online_ugenr" name="ugenr">
								<option value="false">-- Vælg uge --</option>
								<?php
								
								$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								for($i = 0; $i < 65; $i++){
									$date = strtotime("today + ". $i." weeks");
									?>
									<option value="<?php echo date("W", $date) . "-" . date("y", $date); ?>"><?php echo "Uge " . date("W", $date) . " - " . date("Y", $date); ?></option>
									<?php
								}
								?>
							</select>
							
						</td>
					</tr>
					<tr class="placeringsrakke">
						<td colspan="2"><p>Placering:</p></td>
					</tr>
					<tr class="placeringsrakke">
						<td colspan="2">
							<select name="placering" class="placeringsselect">
								<optgroup label="Online">
									<option value="false">-- Vælg placering --</option>
									<option value="30">Topbanner</option>
									<option value="12">Forsidebannere</option>
									<option value="31">Sidebanner 333x300</option>
									<option value="32">Sidebanner 333x600</option>
									<option value="33">Sidebanner 333x300</option>
								</optgroup>
								<optgroup label="Nyhedsbrev">
									<option value="21">Topbanner</option>
									<option value="22">Std. banner 1</option>
									<option value="23">Std. banner 2</option>
									<option value="24">Std. banner 3</option>
								</optgroup>
							</select>
							
						</td>
					</tr>
					<tr class="antalplaceringsrakke">
						<td colspan="2"><p>Antal visninger:</p></td>
					</tr>
					<tr class="antalplaceringsrakke">
						<td colspan="2">
							<select name="antal" class="antalplaceringsselect">
									<option value="1">1 visning</option>
									<option value="2">2 visninger</option>
									<option value="3">3 visninger</option>
							</select>
							
						</td>
					</tr>
					<tr>
						<td colspan="2"><p>Note:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="note"><?php echo $ordre["note"]; ?></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Tilknyt online annonce" />
					</tr>
			
			
				</table>
			</form>
		</div>
	
	</div>


</body>
</html>