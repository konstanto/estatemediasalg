<?php include("login_kontrol.php"); include("../database.php"); 
	
	$id = $_GET["id"];
	$maned = $_GET["maned"];
	
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder ">
		<div class="opretboks">
			<div class="header">
				<a href="tilknyt_online.php?maned=<?php echo $maned; ?>&ordreid=<?php echo $id; ?>" class="menu"><h1>Tilbage</h1></a>
				<h1>Tilknyt online annonce i flere uger</h1>
			</div>
			<form method="post" action="tilknyt_flere_uger_online_send.php">
				<input type="hidden" name="ordreid" value="<?php echo $id; ?>" />
				<input type="hidden" name="oldmaned" value="<?php echo $maned; ?>" />
				<table>
					<?php if(isset($_GET["fejl"])){ ?><tr><td colspan="2"><h1>Der skete en fejl under oprettelsen, da ikke alle de angivne uger er ledige for denne placering. Der er derfor ikke blevet oprettet nogle online annoncer</h1></td></tr><?php } ?>
					<tr>
						<td><p>Startuge:</p></td>
						<td><p>Antal uger:</p></td>
					</tr>
					<tr>
						<td>
							<select class="online_flere_ugenr" name="ugenr">
								<option value="false">-- Vælg uge --</option>
								<?php
								
								$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								for($i = 0; $i < 72; $i++){
									$date = strtotime("today + ". $i." weeks");
									?>
									<option value="<?php echo date("W", $date) . "-" . date("y", $date); ?>"><?php echo "Uge " . date("W", $date) . " - " . date("Y", $date); ?></option>
									<?php
								}
								?>
							</select>
							
						</td>
						<td><input type="text" name="antal_uger" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Placering:</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="placering" class="placeringsselect">
								<optgroup label="Online">
									<option value="false">-- Vælg placering --</option>
									<option value="30">Topbanner</option>
									<option value="12">Forsidebannere</option>
									<option value="31">Sidebanner 333x300</option>
									<option value="32">Sidebanner 333x600</option>
									<option value="33">Sidebanner 333x300</option>
								</optgroup>
								<optgroup label="Nyhedsbrev">
									<option value="21">Topbanner</option>
									<option value="22">Std. banner 1</option>
									<option value="23">Std. banner 2</option>
									<option value="24">Std. banner 3</option>
								</optgroup>
							</select>
							
						</td>
					</tr>
					<tr>
						<td colspan="2"><p>Note:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="note"><?php echo $ordre["note"]; ?></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Tilknyt online annonce" />
					</tr>
			
			
				</table>
			</form>
		</div>
	
	</div>


</body>
</html>