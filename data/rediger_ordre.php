<?php include("login_kontrol.php"); include("../database.php"); 
	
	$id = $_GET["id"];
	$maned = $_GET["maned"];
	
	$resultat = mysql_query("SELECT * FROM salg_handler WHERE id = '$id'");
	if(!$resultat){
		  die('Could not connect: ' . mysql_error());
		  }
	
	$ordre = mysql_fetch_array($resultat);
	
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>


<script src="../js/datepicker/jquery.ui.core.js"></script>
<script src="../js/datepicker/jquery.ui.datepicker.js"></script>
<script src="../js/datepicker/jquery.ui.datepicker-da.js"></script>
<link rel="stylesheet" href="../js/datepicker/jquery.ui.all.css">
<link rel="stylesheet" href="../js/datepicker/demos.css">
<script type="text/javascript">
$(function() {
		$( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
		
	});
</script>



<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder">
		<div class="opretboks">
			<div class="header">
				<?php if($_GET["brancheguide"] == 1){
					?>
					<a href="../brancheguide/brancheguide.php?periode=<?php echo $_GET["periode"]; ?>" class="menu"><h1>Liste</h1></a>
					<?php
				}
				else if($_GET["genfakturering"] == 1){
					?>
					<a href="../brancheguide/genfakturering.php?periode=<?php echo $_GET["periode"]; ?>" class="menu"><h1>Liste</h1></a>
					<?php
				}
				else {
					?>
					<a href="ordre_maned.php?maned=<?php echo $maned; ?>" class="menu"><h1>Liste</h1></a>
					<?php
				}
				?>
				<h1>Rediger ordre</h1>
			</div>
			<form method="post" action="rediger_ordre_send.php">
				<input type="hidden" name="annoncer" value="<?php echo $_GET["annoncer"]; ?>" />
				<input type="hidden" name="genfakturering" value="<?php echo $_GET["genfakturering"]; ?>" />
				<input type="hidden" name="magasinnr" value="<?php echo $_GET["magasinnr"]; ?>" />
				<input type="hidden" name="idet" value="<?php echo $id; ?>" />
				<input type="hidden" name="oldmaned" value="<?php echo $maned; ?>" />
				<input type="hidden" name="brancheguide" value="<?php echo $_GET["brancheguide"]; ?>" />
				<input type="hidden" name="periode" value="<?php echo $_GET["periode"]; ?>" />
				<table>
					<tr>
						<td colspan="2"><p>Kundenavn:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="kunde" value="<?php echo $ordre["kunde"]; ?>"/></td>
					</tr>
					<tr>
						<td><p>Beløb:</p></td>
						<td><p>Måned:</p></td>
					</tr>
					<tr>
						<td><input type="text" name="belob" value="<?php echo $ordre["ordrebelob"]; ?>" /></td>
						<td>
							<select name="maned">
								<?php
								
								$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								$today = date("Y-m", strtotime('today')) . "-15";
								
								for($i = 0; $i < 12; $i++){
									$date = strtotime("$today + ". $i." months");
									?>
									<option <?php if($ordre["maned"] == date("Y-m", $date) . "-15"){echo "Selected";} ?> value="<?php echo date("Y-m", $date); ?>-15"><?php echo $months[date("n", $date)-1] . " " . date("Y", $date); ?></option>
									<?php
								}
								
								
								?>
								<?php
								
								$months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
								
								$today = date("Y-m", strtotime('today - 1 year')) . "-15";
								
								for($i = 0; $i < 12; $i++){
									$date = strtotime("$today + ". $i." months");
									?>
									<option <?php if($ordre["maned"] == date("Y-m", $date) . "-15"){echo "Selected";} ?> value="<?php echo date("Y-m", $date); ?>-15"><?php echo $months[date("n", $date)-1] . " " . date("Y", $date); ?></option>
									<?php
								}
								
								
								?>
								
								
								
							</select>
						</td>
					</tr>
					<tr>
						<td><p>Sælger:</p></td>
						<td><p>Type:</p></td>
					</tr>
					<tr>
						<td>
							<select name="salger">
								<option <?php if($ordre["salgerid"] == 2){echo "selected";} ?> value="2">Michael</option>
								<option <?php if($ordre["salgerid"] == 3){echo "selected";} ?> value="3">Katja</option>
								<option <?php if($ordre["salgerid"] == 1){echo "selected";} ?> value="1">Adnan</option>
							</select>
						</td>
						<td>
							<select class="typevalger" name="type">
								<option <?php if($ordre["type"] == 1){echo "selected";} ?> value="1">Annonce - print</option>
								<option <?php if($ordre["type"] == 4){echo "selected";} ?> value="4">Annonce - web</option>
								<option <?php if($ordre["type"] == 5){echo "selected";} ?> value="5">Brancheguide</option>
								<option <?php if($ordre["type"] == 2){echo "selected";} ?> value="2">Reklame</option>
								<option <?php if($ordre["type"] == 3){echo "selected";} ?> value="3">Sponsor</option>
								<option <?php if($ordre["type"] == 6){echo "selected";} ?> value="6">MoreCard</option>
								<option <?php if($ordre["type"] == 7){echo "selected";} ?> value="7">Diverse</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2"><p>Ordrenummer:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="ordrenummer" value="<?php echo $ordre["ordrenummer"]; ?>" /></td>
					</tr>
					<tr class="magasinrakke">
						<td><p>Magasinnummer:</p></td>
						<td><p>Størrelse:</p></td>
					</tr>
					<tr class="magasinrakke">
						<td>
							<select name="magasinnr">
								<?php
								$resultat = mysql_query("SELECT * FROM salg_magasiner WHERE id = '1'");
								if(!$resultat){
									  die('Could not connect: ' . mysql_error());
									  }
							
								$magasiner = mysql_fetch_array($resultat);
								
								$magasiner = unserialize($magasiner["magasinarray"]);
								
								if(!in_array($ordre["magasinnr"], $magasiner)){
									?>
									<option selected value="<?php echo $brugt = $ordre["magasinnr"]; ?>">Nr. <?php echo substr($ordre["magasinnr"], 2); ?> - 20<?php echo substr($ordre["magasinnr"], 0, 2); ?></option>
									<?php
								}
								
								foreach($magasiner as $magasin){
									?>
									<option <?php if($ordre["magasinnr"] == $magasin) {echo "selected";} ?> value="<?php echo $magasin; ?>">Nr. <?php echo substr($magasin, 2); ?> - 20<?php echo substr($magasin, 0, 2); ?></option>
								<?php
								}
								?>
							</select>
						</td>
						<td>
							<select name="storrelse">
								<option <?php if($ordre["storrelse"] == 0.125) {echo "selected";} ?> value="0.125">0.125 side</option>
								<option <?php if($ordre["storrelse"] == 0.25) {echo "selected";} ?> value="0.25">0.25 side</option>
								<option <?php if($ordre["storrelse"] == 0.5) {echo "selected";} ?> value="0.5">0.5 side</option>
								<option <?php if($ordre["storrelse"] == 1) {echo "selected";} ?> value="1">1 side</option>
								<option <?php if($ordre["storrelse"] == 2) {echo "selected";} ?> value="2">2 side</option>
							</select>
						</td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2"><p>Status:</p></td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2">
							<select name="status" style="width: 101%;">
								<option <?php if($ordre["status"] == 0) {echo "selected";} ?> value="0">-</option>
								<option <?php if($ordre["status"] == 1) {echo "selected";} ?> value="1">Kunde ikke kontaktet</option>
								<option <?php if($ordre["status"] == 2) {echo "selected";} ?> value="2">Kunde kontaktet</option>
								<option <?php if($ordre["status"] == 3) {echo "selected";} ?> value="3">Materiale modtaget</option>
								<option <?php if($ordre["status"] == 4) {echo "selected";} ?> value="4">Materiale godkendt</option>
							</select>
						</td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2"><p>Note:</p></td>
					</tr>
					<tr class="magasinrakke">
						<td colspan="2"><textarea name="magasinnote"><?php echo $ordre["note"]; ?></textarea></td>
					</tr>
					
					<tr class="brancheguiderakke">
						<td><p>Vælg brancheguide</p></td>
						<td><p>Startmåned:</p></td>
					</tr>
					<tr class="brancheguiderakke">
						<td>
							<select name="brancheguideid" style="width: 101%;">
								<?php 
								
								$resultat2 = mysql_query("SELECT * FROM salg_brancheguide ORDER by navn");
								if(!$resultat2){
									  die('Could not connect: ' . mysql_error());
									  }
								
								while($brancheguide = mysql_fetch_array($resultat2)){
									?>
									<option <?php if($ordre["brancheguideid"] == $brancheguide["id"]){echo "selected";} ?> value="<?php echo $brancheguide["id"]; ?>"><?php echo $brancheguide["navn"]; ?></option>
									<?php
								}
								?>
								
							</select>
						</td>
						<td><input class="datepicker" type="date" name="brancheguidestart" value="<?php echo $ordre["brancheguidestart"]; ?>" /></td>
					</tr>
					
					<tr class="brancheguiderakke">
						<td colspan="2"><p>Note:</p></td>
					</tr>
					<tr class="brancheguiderakke">
						<td colspan="2"><textarea name="note"><?php echo $ordre["note"]; ?></textarea></td>
					</tr>
					
					<tr>
						<td></td>
						<td class="tilknyt_knap"><a href="tilknyt_online.php?maned=<?php echo $maned; ?>&ordreid=<?php echo $id; ?>" class="menu_right"><h1>Tilknyt online annonce</h1></a></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Gem ændringer" />
					</tr>
			
			
				</table>
			</form>
		</div>
		
		<div class="placeringstable">
			<h1>Online annoncering</h1>
			<table style="width: 100%;">
				<tr>
					<th><p>Placering</p></th>
					<th><p>Ugenr.</p></th>
					<th><p>Note</p></th>
					<th><p></p></th>
					<th><p></p></th>
				</tr>
			    <?php
			    
			    $placeringer_online_beskrivelse = array("11" => "Online: Megaboard", "12" => "Online: Forsidebanner", "13" => "Online: Venstre 1", "14" => "Online: Venstre 2", "15" => "Online: Venstre 3", "16" => "Online: Højre 1", "17" => "Online: Højre 2", "18" => "Online: Højre 3", "19" => "Online: Jobmarked 1", "110" => "Online: Jobmarked 2", "111" => "Online: Jobmarked 3", "21" => "Nyhedsbrev: Topbanner", "22" => "Nyhedsbrev: Std. banner 1", "23" => "Nyhedsbrev: Std. banner 2", "24" => "Nyhedsbrev: Std. banner 3");
			    
			    
			    $resultat = mysql_query("SELECT * FROM salg_online WHERE ordreid = '$id'");
			    if(!$resultat){
			    	  die('Could not connect: ' . mysql_error());
			    	  }
			    
			    while($online = mysql_fetch_array($resultat)){
			    	?>
			    	<tr>
			    		<td><p><?php echo $placeringer_online_beskrivelse[$online["placering"]]; ?></p></td>
			    		<td><p>Uge <?php 
			    		$uge = explode("-", $online["uge"]); 
			    		echo $uge[0] . " - 20" . $uge[1];
			    		?></p></td>
			    		<td><p><?php echo $online["note"]; ?></p></td>
			    		<td><p><a href="../rediger_online.php?id=<?php echo $online["id"]; ?>&ugenr=<?php echo $online["uge"]; ?>&ordreoversigt=1&maned=<?php echo $maned; ?>&ordreid=<?php echo $id; ?>">Rediger</a></p></td>
			    		<td><p><a href="slet_online.php?id=<?php echo $online["id"]; ?>&maned=<?php echo $maned; ?>&ordreid=<?php echo $id; ?>">Slet</a></p></td>
			    	</tr>
			    	<?php
			    }
			    
			    ?>
			</table>
		</div>
	</div>


</body>
</html>