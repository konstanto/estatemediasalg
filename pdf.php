<?php
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2010-08-08
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

require_once('tcpdf_php4/config/lang/eng.php');
require_once('tcpdf_php4/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	function Header() {
		
// Set font
		$this->SetFont('helvetica', 'B', 12);
		
		$this->SetY(12);
		// Title
		$this->Cell(0, 0, "", 0, false, 'L', 0, '', 0, false, 'S', 'M');
		$this->SetY(17);
		$this->Cell(0, 0, '', 0, false, 'm', 0, '', 0, false, 'S', 'S');
		
		// Logo
		$image_file = K_PATH_IMAGES.'estate_media.jpg';
		$this->Image($image_file, 155, 13, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		
	}

	// Page footer
	function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-22);
		// Set font
		$this->SetFont('helvetica', 'B', 12);
		// Page number
		$this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		// Position at 15 mm from bottom
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Estate Media');
$pdf->SetTitle('Salg - Estate Media DK');
$pdf->SetSubject('Salg - Estate Media DK');
$pdf->SetKeywords('Salg - Estate Media DK');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);


$pdf->SetFont('Helvetica', '', 10);
// add a page
$pdf->AddPage();

$dato = '2013-09-15';

$forbindelse = mysql_connect("mysql23.unoeuro.com", "estatekonfe_dk", "super3140");
mysql_select_db("estatekonference_dk_db", $forbindelse);

$budget1 = array();
$budget2 = array();
$budget3 = array();
$realiseret1 = array();
$realiseret2 = array();
$realiseret3 = array();
$samletbudget = array();
$samletrealiseret = array();

$result = mysql_query("SELECT * FROM salg_budget WHERE maned = '$dato' AND salgerid = '1'");
while($budget = mysql_fetch_array($result)){
	$budget1["annoncer"] = $budget["annoncer"];
	$budget1["reklame"] = $budget["reklame"];
	$budget1["sponsor"] = $budget["sponsor"];
	$budget1["sum"] = $budget1["annoncer"] + $budget1["reklame"] + $budget1["sponsor"];
	
}

$result = mysql_query("SELECT * FROM salg_budget WHERE maned = '$dato' AND salgerid = '2'");
while($budget = mysql_fetch_array($result)){
	$budget2["annoncer"] = $budget["annoncer"];
	$budget2["reklame"] = $budget["reklame"];
	$budget2["sponsor"] = $budget["sponsor"];
	$budget2["sum"] = $budget2["annoncer"] + $budget2["reklame"] + $budget2["sponsor"];
}

$result = mysql_query("SELECT * FROM salg_budget WHERE maned = '$dato' AND salgerid = '3'");
while($budget = mysql_fetch_array($result)){
	$budget3["annoncer"] = $budget["annoncer"];
	$budget3["reklame"] = $budget["reklame"];
	$budget3["sponsor"] = $budget["sponsor"];
	$budget3["sum"] = $budget3["annoncer"] + $budget3["reklame"] + $budget3["sponsor"];
}

//samlet
$samletbudget["annoncer"] = $budget1["annoncer"] + $budget2["annoncer"] + $budget3["annoncer"];
$samletbudget["reklame"] = $budget1["reklame"] + $budget2["reklame"] + $budget3["reklame"];
$samletbudget["sponsor"] = $budget1["sponsor"] + $budget2["sponsor"] + $budget3["sponsor"];


//sælger 1 realiseret
$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '1' AND type = '1' OR type = '4' OR type = '5'");
$realiseret = mysql_fetch_array($result);
$realiseret1["annoncer"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '1' AND type = '2'");
$realiseret = mysql_fetch_array($result);
$realiseret1["reklame"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '1' AND type = '3'");
$realiseret = mysql_fetch_array($result);
$realiseret1["sponsor"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$realiseret1["sum"] = $realiseret1["annoncer"] + $realiseret1["reklame"] + $realiseret1["sponsor"];
$diff1 = $realiseret1["sum"] - $budget1["sum"];



//sælger 2 realiseret
$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '2' AND type = '1' OR type = '4' OR type = '5'");
$realiseret = mysql_fetch_array($result);
$realiseret2["annoncer"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '2' AND type = '2'");
$realiseret = mysql_fetch_array($result);
$realiseret2["reklame"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '2' AND type = '3'");
$realiseret = mysql_fetch_array($result);
$realiseret2["sponsor"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$realiseret2["sum"] = $realiseret2["annoncer"] + $realiseret2["reklame"] + $realiseret2["sponsor"];
$diff2 = $realiseret2["sum"] - $budget2["sum"];


//sælger 3 realiseret
$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '3' AND type = '1' OR type = '4' OR type = '5'");
$realiseret = mysql_fetch_array($result);
$realiseret3["annoncer"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '3' AND type = '2'");
$realiseret = mysql_fetch_array($result);
$realiseret3["reklame"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$result = mysql_query("SELECT SUM(ordrebelob) as sum FROM salg_handler WHERE maned = '$dato' AND salgerid = '3' AND type = '3'");
$realiseret = mysql_fetch_array($result);
$realiseret3["sponsor"] = ($realiseret["sum"] != "") ? $realiseret["sum"] : 0;

$realiseret3["sum"] = $realiseret3["annoncer"] + $realiseret3["reklame"] + $realiseret3["sponsor"];
$diff3 = $realiseret3["sum"] - $budget3["sum"];

//samlet
$samletrealiseret["annoncer"] = $realiseret1["annoncer"] + $realiseret2["annoncer"] + $realiseret3["annoncer"];
$samletrealiseret["reklame"] = $realiseret1["reklame"] + $realiseret2["reklame"] + $realiseret3["reklame"];
$samletrealiseret["sponsor"] = $realiseret1["sponsor"] + $realiseret2["sponsor"] + $realiseret3["sponsor"];




$sumbudget = $samletbudget["annoncer"] + $samletbudget["reklame"] + $samletbudget["sponsor"];
$sumrealiseret = $samletrealiseret["annoncer"] + $samletrealiseret["reklame"] + $samletrealiseret["sponsor"];
$samletdiff = $sumrealiseret - $sumbudget;

if($diff1 < 0){
	$diff1farve = "color: red";
}

if($diff2 < 0){
	$diff2farve = "color: red";
}

if($diff3 < 0){
	$diff3farve = "color: red";
}

if($samletdiff < 0){
	$samletdifffarve = "color: red";
}























$content = <<<EOD
	<table>
	 <tr>
	 	<td colspan="5"><p style="text-align: center;">September</p></td>
	 </tr>
	 <tr>
	 	<td style="border-bottom: 1px solid black;"></td>
	 	<td style="border-bottom: 1px solid black;">Michael</td>
	 	<td style="border-bottom: 1px solid black;">Katja</td>
	 	<td style="border-bottom: 1px solid black;">Adnan</td>
	 	<td style="border-bottom: 1px solid black;">Samlet</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold;">Bud. annoncer</td>
	 	<td>$budget2[annoncer]</td>
	 	<td>$budget3[annoncer]</td>
	 	<td>$budget1[annoncer]</td>
	 	<td>$samletbudget[annoncer]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Realiseret</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret2[annoncer]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret3[annoncer]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret1[annoncer]</td>
	 	<td style="border-bottom: 1px solid black;">$samletrealiseret[annoncer]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold;">Bud. Online</td>
	 	<td>$budget2[reklame]</td>
	 	<td>$budget3[reklame]</td>
	 	<td>$budget1[reklame]</td>
	 	<td>$samletbudget[reklame]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Realiseret</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret2[reklame]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret3[reklame]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret1[reklame]</td>
	 	<td style="border-bottom: 1px solid black;">$samletrealiseret[reklame]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold;">Bud. Sponsor</td>
	 	<td>$budget2[sponsor]</td>
	 	<td>$budget3[sponsor]</td>
	 	<td>$budget1[sponsor]</td>
	 	<td>$samletbudget[sponsor]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Realiseret</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret2[sponsor]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret3[sponsor]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret1[sponsor]</td>
	 	<td style="border-bottom: 1px solid black;">$samletrealiseret[sponsor]</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Sum budget</td>
	 	<td style="border-bottom: 1px solid black;">$budget2[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$budget3[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$budget1[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$sumbudget</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Sum realiseret</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret2[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret3[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$realiseret1[sum]</td>
	 	<td style="border-bottom: 1px solid black;">$sumrealiseret</td>
	</tr>
	<tr>
	 	<td style="font-weight: bold; border-bottom: 1px solid black;">Difference</td>
	 	<td style="font-weight: bold; border-bottom: 1px solid black; $diff2farve">$diff2</td>
	 	<td style="font-weight: bold; border-bottom: 1px solid black; $diff3farve">$diff3</td>
	 	<td style="font-weight: bold; border-bottom: 1px solid black; $diff1farve">$diff1</td>
	 	<td style="font-weight: bold; border-bottom: 1px solid black; $samletdifffarve">$samletdiff</td>
	</tr>
	
	
	</table>
EOD;

// ---------------------------------------------------------
$forbindelse = mysql_connect("mysql23.unoeuro.com", "estatekonfe_dk", "super3140");
mysql_select_db("estatekonference_dk_db", $forbindelse);


$pdf->writeHTML($content, true, false, false, false, '');

//Close and output PDF document
$pdf->Output('Salg.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
