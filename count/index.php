<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Line Counter</title>
        <style type="text/css">
            ul {
                list-style: none;
                padding-left: 20px;
                font-size: 13px;
            }
            .folder {
                font-family: serif;
                font-size:  16px;
                font-weight: bold;
                text-decoration: underline;
            }
            .file {
                font-family: sans-serif;
                font-size: 13px;
                line-height: 13px;
                border-left: 2px solid #ccc;
                padding: 0 5px;
                margin: 5px 0;
            }
        </style>
    </head>
    <body>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get">
            Directory: <input type="text" name="dir" /> <input type="submit" value="Submit" />
        </form>
        <?php
        if(isset($_GET['dir'])) {
            $src = __DIR__ . '/protected/';
            require $src . 'Folder.php';
            require $src . 'File.php';
            require $src . 'Option.php';
            require $src . 'Html.php';

            echo Html::b($_GET['dir']) . '<hr />';

            //Use GET so this script could be reused elsewhere
            //Set to user defined options or default one
            $options = isset($_GET['options']) ? $_GET['options'] : array(
                'ignoreFolders' => explode(', ', '.svn, nbproject, Classes, Classes2, amcharts_2, Documentation, tcpdf_php4, Zend'),
                'ignoreFiles' => explode(', ', 'jquery.js, jquery-1.5.min.js'),
                'extensions' => explode(', ', 'php, js, css'),
                'whitespace' => false,
                'comments' => false,
                    );


            //Scan user defined directory
            $folder = new Folder($_GET['dir'], new Option($options));
            $folder->init();

            $lines = $folder->getLines();
            $whitespace = $folder->getWhitespace();
            $comments = $folder->getComments();

            //Prepares the summary
            $result = 'Lines: ' . Html::b($lines);
            if(!$options['whitespace'])
                $result .= ', Whitespace: ' . Html::b($whitespace);
            if(!$options['comments'])
                $result .= ', Comments: ' . Html::b($comments);
            if(!$options['whitespace'] && !$options['comments']) {
                $result .= ', Percentage: ' . Html::b(round($lines / ($lines + $whitespace + $comments) * 100)) . '%';
            }

            //Prints the summary
            echo '<hr />' . Html::p($result);
        }
        ?>
    </body>
</html>