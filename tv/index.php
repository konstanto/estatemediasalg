<?php
if ($_GET["pass"] != "estatemediapass") {
    header("Location: ../index.php");
}
include("../database.php");
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <!--[if lt IE 12]>
    <link rel="stylesheet" type="text/css" href="../js/html5/ie.css"/>
    <![endif]-->
    <script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../js/Chart.js"></script>

    <script type="text/javascript" src="js/js.js"></script>

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Salg - Estate Media</title>
</head>
<body>
<div class="page">
    <div class="content">
        <?php
        $months = array("Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");

        for ($i = 0; $i < 2; $i++) {

            // Vi kører igennem 6 måneder
            if (date("j") > 15) {
                $date = strtotime("today - 10 days + " . $i . " months");
            } else {
                $date = strtotime("today + " . $i . " months");
            }
            $datotilsql = date("Y-m", $date) . "-15";
            ?>
            <div class="maned">
                <h1 class="manedsnavn"><?php echo $months[date("n", $date) - 1]; ?></h1>
                <div class="top">
                    <div class="topdel beskrivelse">
                        <div class="beskrivelsesdel felt"></div>
                        <div class="beskrivelsesdel felt">
                            <p>Bud. annoncer</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Realiseret</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Bud. Online</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Realiseret</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Bud. Sponsor</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Realiseret</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Sum budget</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Sum realiseret</p>
                        </div>
                        <div class="beskrivelsesdel felt">
                            <p>Difference</p>
                        </div>
                    </div>

                    <?php

                    //Laver så man ikke behøver at skrive kolonnerne for alle sælgerne enkeltvis. De skal dog stå i bestemt rækkefølge, så derfor bliver de konstrueret ud fra nedentående array

                    $salgere = array(2, 3, 1);
                    $sellerNames = array("Adnan", "Michael", "Katja");
                    $counter = 1;

                    foreach ($salgere as $salgerid) {
                        ?>

                        <div class="topdel salger">
                            <?php
                            $resultat = mysql_query("SELECT * FROM salg_budget WHERE maned = '$datotilsql' AND salgerid = '$salgerid'");
                            $salger_budget = mysql_fetch_array($resultat);
                            $sum_realiseret = 0;
                            ?>

                            <div class="salgsdel navn felt">
                                <h2><?php echo $sellerNames[$salgerid - 1]; ?></h2>
                            </div>

                            <div class="salgsdel todel">
                                <div class="to">
                                    <p><?php echo number_format($salger_budget["annoncer"], 0, ',', '.'); ?></p>
                                </div>
                                <div class="to">
                                    <p><?php

                                        $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type != 2 AND type!= 3");
                                        $sum_lokal = mysql_fetch_array($resultat);

                                        $sum_realiseret += $sum_lokal["count"];

                                        echo number_format($sum_lokal["count"], 0, ',', '.');
                                        ?></p>
                                </div>
                            </div>


                            <div class="salgsdel todel">
                                <div class="to">
                                    <p><?php echo number_format($salger_budget["reklame"], 0, ',', '.'); ?></p>
                                </div>
                                <div class="to">
                                    <p><?php

                                        $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type = 2");
                                        $sum_lokal = mysql_fetch_array($resultat);

                                        $sum_realiseret += $sum_lokal["count"];

                                        echo number_format($sum_lokal["count"], 0, ',', '.');
                                        ?></p>
                                </div>
                            </div>


                            <div class="salgsdel todel">
                                <div class="to">
                                    <p><?php echo number_format($salger_budget["sponsor"], 0, ',', '.'); ?></p>
                                </div>
                                <div class="to">
                                    <p><?php

                                        $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND salgerid = '$salgerid' AND type = 3");
                                        $sum_lokal = mysql_fetch_array($resultat);

                                        $sum_realiseret += $sum_lokal["count"];

                                        echo number_format($sum_lokal["count"], 0, ',', '.');
                                        ?></p>
                                </div>
                            </div>
                            <div class="salgsdel">
                                <p><?php echo number_format($sum_budget = (int)$salger_budget["annoncer"] * 1 + (int)$salger_budget["reklame"] * 1 + (int)$salger_budget["sponsor"] * 1, 0, ',', '.'); ?></p>
                            </div>
                            <div class="salgsdel">
                                <p><?php echo number_format($sum_realiseret, 0, ',', '.'); ?></p>
                            </div>
                            <div class="salgsdel diff <?php if (($diff = $sum_realiseret - $sum_budget) < 0) {
                                echo "negativ";
                            } ?>">
                                <p><?php echo number_format(($diff), 0, ',', '.'); ?></p>
                            </div>
                        </div>
                        <?php
                        $counter++;
                    }
                    ?>

                    <div class="topdel salger">
                        <?php
                        //Det samlede resultat
                        $resultat = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE maned = '$datotilsql'");
                        $salger_budget = mysql_fetch_array($resultat);
                        $sum_realiseret = 0;
                        ?>

                        <div class="salgsdel navn felt">
                            <h2>Samlet</h2>
                        </div>
                        <div class="salgsdel todel">
                            <div class="to">
                                <p><?php echo number_format($salger_budget["annoncebudget"], 0, ',', '.'); ?></p>
                            </div>
                            <div class="to">
                                <p><?php

                                    $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type != 2 AND type != 3");
                                    $sum_lokal = mysql_fetch_array($resultat);

                                    $sum_realiseret += $sum_lokal["count"];

                                    echo number_format($sum_lokal["count"], 0, ',', '.');
                                    ?></p>
                            </div>
                        </div>
                        <div class="salgsdel todel">
                            <div class="to">
                                <p><?php echo number_format($salger_budget["reklamebudget"], 0, ',', '.'); ?></p>
                            </div>
                            <div class="to">
                                <p><?php

                                    $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type = 2");
                                    $sum_lokal = mysql_fetch_array($resultat);

                                    $sum_realiseret += $sum_lokal["count"];

                                    echo number_format($sum_lokal["count"], 0, ',', '.');
                                    ?></p>
                            </div>
                        </div>
                        <div class="salgsdel todel">
                            <div class="to">
                                <p><?php echo number_format($salger_budget["sponsorbudget"], 0, ',', '.'); ?></p>
                            </div>
                            <div class="to">
                                <p><?php

                                    $resultat = mysql_query("SELECT SUM(ordrebelob) as count FROM salg_handler WHERE maned = '$datotilsql' AND type = 3");
                                    $sum_lokal = mysql_fetch_array($resultat);

                                    $sum_realiseret += $sum_lokal["count"];

                                    echo number_format($sum_lokal["count"], 0, ',', '.');
                                    ?></p>
                            </div>
                        </div>
                        <div class="salgsdel">
                            <p><?php echo number_format($sum_budget = (int)$salger_budget["annoncebudget"] * 1 + (int)$salger_budget["reklamebudget"] * 1 + (int)$salger_budget["sponsorbudget"] * 1, 0, ',', '.'); ?></p>
                        </div>
                        <div class="salgsdel">
                            <p><?php echo number_format($sum_realiseret, 0, ',', '.'); ?></p>
                        </div>
                        <div class="salgsdel diff <?php if (($diff = $sum_realiseret - $sum_budget) < 0) {
                            echo "negativ";
                        } ?>">
                            <p><?php echo number_format(($diff), 0, ',', '.'); ?></p>
                        </div>
                    </div>

                </div>

                <div class="bottom">
                    <div class="barchart">
                        <h1><?php echo $months[date("n", $date) - 1]; ?></h1>

                        <canvas id="bar<?php echo $i; ?>" height="710" width="80"></canvas>
                        <script type="text/javascript">
                            <?php
                            //Vi tester for om der er data for denne måned. Hvis ikke skal den bare udskrive 0
                            if ($sum_budget != "") {
                                $sum_budget = $sum_budget / 1000;
                            } else {
                                $sum_budget = "0";
                            }

                            if ($sum_realiseret != "") {
                                $sum_realiseret = $sum_realiseret / 1000;
                            } else {
                                $sum_realiseret = "0";
                            }
                            ?>
                            var barChartData = {
                                labels: [""],
                                datasets: [
                                    {
                                        fillColor: "rgba(0, 100, 204 ,0.5)",
                                        strokeColor: "rgba(220,220,220,1)",
                                        data: ['<?php echo $sum_budget; ?>']
                                    },
                                    {
                                        fillColor: "rgba(23, 148, 0 ,0.5)",
                                        strokeColor: "rgba(220,220,220,1)",
                                        data: ['<?php echo $sum_realiseret; ?>']
                                    }
                                ]

                            }

                            <?php $highest = max($sum_budget, $sum_realiseret); ?>
                            new Chart(document.getElementById("bar<?php echo $i; ?>").getContext("2d")).Bar(barChartData, {
                                scaleSteps: 14,
                                scaleStepWidth: 50,
                                animation: false
                            });
                        </script>


                        <p class="bud">Bud.</p>
                        <p class="rea">Rea.</p>
                    </div>

                    <div class="grafer">
                        <div class="graf">
                            <div class="graftext">
                                <h1>Michael</h1>
                                <p class="bud">Sum bud.</p>
                                <p class="rea">Sum Rea.</p>

                            </div>
                            <canvas id="canvas<?php echo $i; ?>_1" height="180" width="290" title="hej1"></canvas>
                        </div>
                        <div class="graf">
                            <div class="graftext">
                                <h1>Katja</h1>
                                <p class="bud">Sum bud.</p>
                                <p class="rea">Sum Rea.</p>

                            </div>
                            <canvas id="canvas<?php echo $i; ?>_2" height="180" width="290" title="hej2"></canvas>
                        </div>
                        <div class="graf">
                            <div class="graftext">
                                <h1>Adnan</h1>
                                <p class="bud">Sum bud.</p>
                                <p class="rea">Sum Rea.</p>

                            </div>
                            <canvas id="canvas<?php echo $i; ?>_3" height="180" width="290" title="hej3"></canvas>

                        </div>
                        <div class="graf">
                            <div class="graftext">
                                <h1>Samlet</h1>
                                <p class="bud">Sum bud.</p>
                                <p class="rea">Sum Rea.</p>
                            </div>
                            <canvas id="canvas<?php echo $i; ?>_4" height="180" width="290"></canvas>
                        </div>


                        <script type="text/javascript">

                            <?php

                            $salgere = array(2, 3, 1);
                            $counter = 1;

                            foreach($salgere as $salgerid){


                            //Vi laver arrays for alle værdierne i de kommende 6 måneder fra udgangspunkt
                            $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");

                            $maned_navn = array();
                            $maned_budget = array();
                            $maned_realiseret = array();

                            for ($ii = 0; $ii < 6; $ii++) {
                                $date = strtotime("$datotilsql + " . $ii . " months");
                                $chartdato = date("Y-m", $date) . "-15";

                                $maned_navn[] = $months_short[date("n", $date) - 1];


                                $resultat5 = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE salgerid = '$salgerid' AND maned = '$chartdato'");
                                $budgettet = mysql_fetch_array($resultat5);

                                $maned_budget[] = ($budgettet["reklamebudget"] + $budgettet["annoncebudget"] + $budgettet["sponsorbudget"]);


                                $resultat5 = mysql_query("SELECT SUM(ordrebelob) as ordrebelob FROM salg_handler WHERE salgerid = '$salgerid' AND maned = '$chartdato'");
                                $real = mysql_fetch_array($resultat5);

                                if ($real["ordrebelob"] != "") {
                                    $maned_realiseret[] = $real["ordrebelob"];
                                } else {
                                    $maned_realiseret[] = 0;
                                }
                            }
                            ?>

                            var lineChartData = {
                                labels: ["<?php echo $maned_navn[0]; ?>", "<?php echo $maned_navn[1]; ?>", "<?php echo $maned_navn[2]; ?>", "<?php echo $maned_navn[3]; ?>", "<?php echo $maned_navn[4]; ?>", "<?php echo $maned_navn[5]; ?>"],
                                datasets: [
                                    {
                                        fillColor: "rgba(220,220,220,0)",
                                        strokeColor: "rgba(0, 100, 204 ,0.5)",
                                        pointColor: "rgba(220,220,220,1)",
                                        pointStrokeColor: "#fff",
                                        data: [<?php echo $maned_budget[0]; ?>, <?php echo $maned_budget[1]; ?>, <?php echo $maned_budget[2]; ?>, <?php echo $maned_budget[3]; ?>, <?php echo $maned_budget[4]; ?>, <?php echo $maned_budget[5]; ?>]
                                    },
                                    {
                                        fillColor: "rgba(151,187,205,0)",
                                        strokeColor: "rgba(23, 148, 0 ,0.5)",
                                        pointColor: "rgba(151,187,205,1)",
                                        pointStrokeColor: "#fff",
                                        data: [<?php echo $maned_realiseret[0]; ?>, <?php echo $maned_realiseret[1]; ?>, <?php echo $maned_realiseret[2]; ?>, <?php echo $maned_realiseret[3]; ?>, <?php echo $maned_realiseret[4]; ?>, <?php echo $maned_realiseret[5]; ?>]
                                    }
                                ]

                            }


                            var myLine = new Chart(document.getElementById("canvas<?php echo $i; ?>_<?php echo $counter; ?>").getContext("2d")).Line(lineChartData, {
                                scaleSteps: 10,
                                scaleStepWidth: 30000,
                                animation: false
                            });
                            <?php
                            $counter++;
                            }
                            ?>


                            <?php


                            //Vi laver arrays for alle værdierne i de kommende 6 måneder fra udgangspunkt
                            $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");

                            $maned_navn = array();
                            $maned_budget = array();
                            $maned_realiseret = array();

                            for ($ii = 0; $ii < 6; $ii++) {
                                $date = strtotime("$datotilsql + " . $ii . " months");
                                $chartdato = date("Y-m", $date) . "-15";

                                $maned_navn[] = $months_short[date("n", $date) - 1];


                                $resultat5 = mysql_query("SELECT SUM(reklame) as reklamebudget, SUM(annoncer) as annoncebudget, SUM(sponsor) as sponsorbudget FROM salg_budget WHERE maned = '$chartdato'");
                                $budgettet = mysql_fetch_array($resultat5);

                                $maned_budget[] = ($budgettet["reklamebudget"] + $budgettet["annoncebudget"] + $budgettet["sponsorbudget"]);


                                $resultat5 = mysql_query("SELECT SUM(ordrebelob) as ordrebelob FROM salg_handler WHERE maned = '$chartdato'");
                                $real = mysql_fetch_array($resultat5);

                                if ($real["ordrebelob"] != "") {
                                    $maned_realiseret[] = $real["ordrebelob"];
                                } else {
                                    $maned_realiseret[] = 0;
                                }
                            }
                            ?>

                            var lineChartData = {
                                labels: ["<?php echo $maned_navn[0]; ?>", "<?php echo $maned_navn[1]; ?>", "<?php echo $maned_navn[2]; ?>", "<?php echo $maned_navn[3]; ?>", "<?php echo $maned_navn[4]; ?>", "<?php echo $maned_navn[5]; ?>"],
                                datasets: [
                                    {
                                        fillColor: "rgba(220,220,220,0)",
                                        strokeColor: "rgba(0, 100, 204 ,0.5)",
                                        pointColor: "rgba(220,220,220,1)",
                                        pointStrokeColor: "#fff",
                                        data: [<?php echo $maned_budget[0]; ?>, <?php echo $maned_budget[1]; ?>, <?php echo $maned_budget[2]; ?>, <?php echo $maned_budget[3]; ?>, <?php echo $maned_budget[4]; ?>, <?php echo $maned_budget[5]; ?>]
                                    },
                                    {
                                        fillColor: "rgba(151,187,205,0)",
                                        strokeColor: "rgba(23, 148, 0 ,0.5)",
                                        pointColor: "rgba(151,187,205,1)",
                                        pointStrokeColor: "#fff",
                                        data: [<?php echo $maned_realiseret[0]; ?>, <?php echo $maned_realiseret[1]; ?>, <?php echo $maned_realiseret[2]; ?>, <?php echo $maned_realiseret[3]; ?>, <?php echo $maned_realiseret[4]; ?>, <?php echo $maned_realiseret[5]; ?>]
                                    }
                                ]
                            }

                            var myLine = new Chart(document.getElementById("canvas<?php echo $i; ?>_4").getContext("2d")).Line(lineChartData, {
                                scaleSteps: 10,
                                scaleStepWidth: 50000,
                                animation: false
                            });

                        </script>
                    </div>
                </div>
            </div>
        <?php } ?>


        <div class="ar_til_dato">

            <div class="ar_del">
                <?php
                $dato = array();
                $realiseret = array();
                $akumuleretrealiseret = array();
                $budget = array();
                $akumuleretbudget = array();


                $ar = date("Y");
                ?>
                <h1>Samlet omsætning realiseret vs. budget pr måned</h1>
                <div class="beskrivelse">
                    <p class="et">Budget</p>
                    <p class="to">Realiseret</p>
                </div>
                <div class="graf">
                    <canvas id="samlet_omsatning" height="280" width="680"></canvas>


                    <script type="text/javascript">
                        <?php
                        $maneden = date("n");
                        if ($maneden < 4) {
                            $t = 1;
                        } else if ($maneden > 3 && $maneden < 7) {
                            $t = 4;
                        } else if ($maneden > 6 && $maneden < 10) {
                            $t = 7;
                        } else if ($maneden > 8) {
                            $t = 10;
                        }



                        for ($i = $t; $i < $t + 3; $i++) {
                            if ($i < 10) {
                                $maned = "0" . $i;
                            } else {
                                $maned = $i;
                            }


                            $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
                            $dato[] = $months_short[$i - 1];


                            $sqldato = $ar . "-" . $maned . "-15";
                            $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["belob"] != "") {
                                $realiseret[] = $data["belob"];
                            } else {
                                $realiseret[] = "0";
                            }


                            $resultat = mysql_query("SELECT SUM(annoncer) + SUM(reklame) + SUM(sponsor) as tal FROM salg_budget WHERE maned = '$sqldato'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["tal"] != "") {
                                $budget[] = $data["tal"];
                            } else {
                                $budget[] = "0";
                            }
                        }
                        ?>

                        var lineChartData = {
                            labels: [<?php foreach ($dato as $date) {
                                echo "\"$date\", ";
                            } ?>],
                            datasets: [
                                {
                                    fillColor: "rgba(220,220,220,0)",
                                    strokeColor: "rgba(23, 148, 0 ,0.5)",
                                    pointColor: "rgba(220,220,220,1)",
                                    pointStrokeColor: "#fff",
                                    data: [<?php foreach ($budget as $budgetet) {
                                        echo "$budgetet, ";
                                    } ?>]
                                },
                                {
                                    fillColor: "rgba(151,187,205,0)",
                                    strokeColor: "rgba(0, 100, 204 ,0.5)",
                                    pointColor: "rgba(151,187,205,1)",
                                    pointStrokeColor: "#fff",
                                    data: [<?php foreach ($realiseret as $real) {
                                        echo "$real, ";
                                    } ?>]
                                }
                            ]

                        }

                        new Chart(document.getElementById("samlet_omsatning").getContext("2d")).Line(lineChartData, {
                            scaleOverride: false,
                            animation: false
                        });
                    </script>
                </div>
            </div>

            <div class="ar_del">
                <?php
                $dato = array();
                $salger1 = array();
                $salger2 = array();
                $salger3 = array();
                $ar = date("Y");
                ?>
                <h1>Realiseret i % af samlet salg pr. sælgerkode</h1>
                <div class="beskrivelse">
                    <?php
                    $maneden = date("n");
                    if ($maneden < 4) {
                        $t = 1;
                    } else if ($maneden > 3 && $maneden < 7) {
                        $t = 4;
                    } else if ($maneden > 6 && $maneden < 10) {
                        $t = 7;
                    } else if ($maneden > 8) {
                        $t = 10;
                    }

                    for ($i = $t; $i < $t + 3; $i++) {
                        if ($i < 10) {
                            $maned = "0" . $i;
                        } else {
                            $maned = $i;
                        }


                        $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
                        $dato[] = $months_short[$i - 1];


                        $sqldato = $ar . "-" . $maned . "-15";
                        $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '1'");
                        $data = mysql_fetch_array($resultat);
                        if ($data["belob"] != "") {
                            $salger1[] = $data["belob"];
                        } else {
                            $salger1[] = "0";
                        }


                        $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '2'");
                        $data = mysql_fetch_array($resultat);
                        if ($data["belob"] != "") {
                            $salger2[] = $data["belob"];
                        } else {
                            $salger2[] = "0";
                        }

                        $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '3'");
                        $data = mysql_fetch_array($resultat);
                        if ($data["belob"] != "") {
                            $salger3[] = $data["belob"];
                        } else {
                            $salger3[] = "0";
                        }

                    }


                    $salger1Sum = array_sum($salger1);
                    $salger2Sum = array_sum($salger2);
                    $salger3Sum = array_sum($salger3);
                    $samletSum = array_sum($salger1) + array_sum($salger2) + array_sum($salger3);

                    $procent1 = $salger1Sum / $samletSum * 100;
                    $procent2 = $salger2Sum / $samletSum * 100;
                    $procent3 = $salger3Sum / $samletSum * 100;

                    ?>

                    <p class="et">Michael: <br/><span class="procent"><?php echo round($procent2); ?>%</span></p>
                    <p class="to">Katja: <br/><span class="procent"><?php echo round($procent3); ?>%</span></p>
                    <p class="tre">Adnan: <br/><span class="procent"><?php echo round($procent1); ?>%</span></p>
                </div>
                <div class="graf">
                    <canvas id="realiseret_procent" height="280" width="680"></canvas>


                    <script type="text/javascript">
                        <?php
                        $maneden = date("n");
                        if ($maneden < 4) {
                            $t = 1;
                        } else if ($maneden > 3 && $maneden < 7) {
                            $t = 4;
                        } else if ($maneden > 6 && $maneden < 10) {
                            $t = 7;
                        } else if ($maneden > 8) {
                            $t = 10;
                        }

                        for ($i = $t; $i < $t + 3; $i++) {
                            if ($i < 10) {
                                $maned = "0" . $i;
                            } else {
                                $maned = $i;
                            }


                            $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
                            $dato[] = $months_short[$i - 1];


                            $sqldato = $ar . "-" . $maned . "-15";
                            $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '1'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["belob"] != "") {
                                $salger1[] = $data["belob"];
                            } else {
                                $salger1[] = "0";
                            }


                            $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '2'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["belob"] != "") {
                                $salger2[] = $data["belob"];
                            } else {
                                $salger2[] = "0";
                            }

                            $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato' AND salgerid = '3'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["belob"] != "") {
                                $salger3[] = $data["belob"];
                            } else {
                                $salger3[] = "0";
                            }

                        }



                        $salger1Sum = array_sum($salger1);
                        $salger2Sum = array_sum($salger2);
                        $salger3Sum = array_sum($salger3);
                        $samletSum = array_sum($salger1) + array_sum($salger2) + array_sum($salger3);

                        $procent1 = $salger1Sum / $samletSum * 100;
                        $procent2 = $salger2Sum / $samletSum * 100;
                        $procent3 = $salger3Sum / $samletSum * 100;

                        ?>

                        var pieData = [
                            {
                                value: <?php echo $procent2; ?>,
                                color: "rgba(23, 148, 0 ,0.5)"
                            },
                            {
                                value: <?php echo $procent3; ?>,
                                color: "rgba(0, 100, 204 ,0.5)"
                            },
                            {
                                value: <?php echo $procent1; ?>,
                                color: "rgba(255, 193, 0, 0.5)"
                            }

                        ];

                        var myPie = new Chart(document.getElementById("realiseret_procent").getContext("2d")).Pie(pieData, {animation: false});
                    </script>
                </div>
            </div>

            <div class="ar_del">
                <?php
                $dato = array();
                $realiseret = array();
                $akumuleretrealiseret = array();
                $budget = array();
                $akumuleretbudget = array();


                $ar = date("Y");
                ?>
                <h1>Samlet omsætning realiseret vs. budget akkumuleret over perioden</h1>
                <div class="beskrivelse">
                    <p class="et">Budget</p>
                    <p class="to">Realiseret</p>
                </div>
                <div class="graf">
                    <canvas id="samlet_omsatning_akumuleret" height="280" width="680"></canvas>


                    <script type="text/javascript">
                        <?php

                        $maneden = date("n");
                        if ($maneden < 4) {
                            $t = 1;
                        } else if ($maneden > 3 && $maneden < 7) {
                            $t = 4;
                        } else if ($maneden > 6 && $maneden < 10) {
                            $t = 7;
                        } else if ($maneden > 8) {
                            $t = 10;
                        }

                        for ($i = $t; $i < $t + 3; $i++) {
                            if ($i < 10) {
                                $maned = "0" . $i;
                            } else {
                                $maned = $i;
                            }


                            $months_short = array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec");
                            $dato[] = $months_short[$i - 1];


                            $sqldato = $ar . "-" . $maned . "-15";
                            $resultat = mysql_query("SELECT SUM(ordrebelob) as belob FROM salg_handler WHERE maned = '$sqldato'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["belob"] != "") {
                                $realiseret[] = $data["belob"];
                                $akumuleretrealiseret[$i] = $akumuleretrealiseret[$i - 1] + $data["belob"];
                            } else {
                                $realiseret[] = "0";
                                $akumuleretrealiseret[$i] = $akumuleretrealiseret[$i - 1] + 0;
                            }


                            $resultat = mysql_query("SELECT SUM(annoncer) + SUM(reklame) + SUM(sponsor) as tal FROM salg_budget WHERE maned = '$sqldato'");
                            $data = mysql_fetch_array($resultat);
                            if ($data["tal"] != "") {
                                $budget[] = $data["tal"];
                                $akumuleretbudget[$i] = $akumuleretbudget[$i - 1] + $data["tal"];
                            } else {
                                $budget[] = "0";
                                $akumuleretbudget[$i] = $akumuleretbudget[$i - 1] + 0;
                            }
                        }
                        ?>


                        var lineChartData = {
                            labels: [<?php foreach ($dato as $date) {
                                echo "\"$date\", ";
                            } ?>],
                            datasets: [
                                {
                                    fillColor: "rgba(220,220,220,0)",
                                    strokeColor: "rgba(23, 148, 0 ,0.5)",
                                    pointColor: "rgba(220,220,220,1)",
                                    pointStrokeColor: "#fff",
                                    data: [<?php foreach ($akumuleretbudget as $budgetet) {
                                        echo "$budgetet, ";
                                    } ?>]
                                },
                                {
                                    fillColor: "rgba(151,187,205,0)",
                                    strokeColor: "rgba(0, 100, 204 ,0.5)",
                                    pointColor: "rgba(151,187,205,1)",
                                    pointStrokeColor: "#fff",
                                    data: [<?php foreach ($akumuleretrealiseret as $real) {
                                        echo "$real, ";
                                    } ?>]
                                }
                            ]

                        }

                        new Chart(document.getElementById("samlet_omsatning_akumuleret").getContext("2d")).Line(lineChartData, {
                            scaleOverride: false,
                            animation: false
                        });
                    </script>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>