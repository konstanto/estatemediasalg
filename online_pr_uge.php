<?php include("login_kontrol.php"); include("database.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js/js.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="ordrepage magasin">
		<h1 class="menulink"><a href="forside.php">Menu</a></h1>
		
		<div class="ugenrValg">
			<?php
			
			for($i = 0; $i < 27; $i++){
				$date = strtotime("10 weeks ago + ". $i." weeks");
				$sqldate = date("W-y", $date);
				?>
				<a href="online_enkelt_uge.php?ugenr=<?php echo $sqldate; ?>"><p>Uge. <?php echo date("W", $date); ?> - <?php echo date("Y", $date); ?></p></a>
			<?php
			
			}
			?>
		</div>
	</div>
</body>
</html>