function skiftGraf() {
	
	$(".andre_grafer").click(function(){
		var nummer = $(this).attr("rel");
		var sektion = $(this).attr("alt");
		
		$(this).parent().children(".active").removeClass("active");
		$(this).addClass("active");
		
		$(this).parent().parent().children(".active").removeClass("active").addClass("inactive");
		
		$(this).parent().parent().children("#canvas" + sektion + "_" + nummer).addClass("active").removeClass("inactive");
		
		$(this).parent().children("h1").html($(this).html());
	});
}

function scrolling(){
	
	console.log("1");
	var index = 0;
	
	$(".nav .forth").click(function(){		
		console.log("2");
		if(index<(3180/$(window).width()+1)){
			
			index++;
			$(".page").animate({scrollLeft: (480+50)*index},"500");
		}	
	});
	$(".nav .back").click(function(){
		if(index>0){
			index--;
			$(".page").animate({scrollLeft: (480+50)*index},"500");
		}

	});
	
	
	$(document).keydown(function(e) {
       if(e.keyCode == 37) { //LEFT
           if(index>0){
			index--;
			$(".page").animate({scrollLeft: (480+50)*index},"500");
		}
       }
       if(e.keyCode == 39) { //RIGHT
            if(index<=(3180/$(window).width()+1)){
			index++;
			$(".page").animate({scrollLeft: (480+50)*index},"500");
		}
       }
    });
}

function vistype() {
	if($(".typevalger").val() == 1){
			$(".magasinrakke").css("display", "table-row");
			$(".brancheguiderakke").css("display", "none");
		}
		else {
			$(".magasinrakke").css("display", "none");
		}

	if($(".typevalger").val() == 5){
			$(".brancheguiderakke").css("display", "table-row");
			$(".magasinrakke").css("display", "none");
		}
		else {
			$(".brancheguiderakke").css("display", "none");
		}

	
	$(".typevalger").change(function(){
		if($(this).val() == 1){
			$(".magasinrakke").css("display", "table-row");
			$(".brancheguiderakke").css("display", "none");
		}
		else if($(this).val() == 5){
			$(".magasinrakke").css("display", "none");
			$(".brancheguiderakke").css("display", "table-row");
		}
		else {
			$(".magasinrakke").css("display", "none");
			$(".brancheguiderakke").css("display", "none");
		}
	})
}
			
function hentMagasin(){
	$(".magasinselect").change(function(){
		$(this).parent().submit();
	});
}
		
function hentArType(){
	$(".valg_ar_til_dato select").change(function(){
		$(this).parent().submit();
	});
}
    
    
function loaddata(){
$(".printknap").click(function(e){
    	var newWin = window.open('http://estatekonference.dk/salg/print/index.php?e=' + $(this).attr("alt"),'','width=950,height=900');
    	//alert($(this).attr("alt"));
		newWin.focus();
	});
}
    
 
function ledigeOnline(){
	$(".online_ugenr").change(function(){
		$.ajax({
			type: "POST",
			url: "../data/ledige_placeringer.php?ugenr=" + $(".online_ugenr").val(),
			data: 'hej',
			async: true,
			dataType: "html",
			success: function(msg){
				$(".placeringsselect").html(msg);
				$(".placeringsrakke").css("display", "table-row");
				}
	
			});
	});


	$(".placeringsselect").change(function(){
		$.ajax({
			type: "POST",
			url: "../data/antal_ledige_placeringer.php?ugenr=" + $(".online_ugenr").val() + "&placering=" + $(".placeringsselect").val(),
			data: 'hej',
			async: true,
			dataType: "html",
			success: function(msg){
				$(".antalplaceringsselect").html(msg);
				$(".antalplaceringsrakke").css("display", "table-row");
				}
	
			});
		});
		
	$(".online_ugenr").change(function(){
		$.ajax({
			type: "POST",
			url: "../data/antal_ledige_placeringer.php?ugenr=" + $(".online_ugenr").val() + "&placering=" + $(".placeringsselect").val(),
			data: 'hej',
			async: true,
			dataType: "html",
			success: function(msg){
				$(".antalplaceringsselect").html(msg);
				$(".antalplaceringsrakke").css("display", "table-row");
				}
	
			});
		});
}   

function redigerOnline(){
	$(".rediger_online_ugenr").ready(function(){
		$.ajax({
			type: "POST",
			url: "data/ledige_placeringer.php?ugenr=" + $(".rediger_online_ugenr").val(),
			data: 'hej',
			async: true,
			dataType: "html",
			success: function(msg){
				$(".placeringsselect").html(msg);
				$(".placeringsrakke").css("display", "table-row");
				}
	
			});
	});
	
	$(".rediger_online_ugenr").change(function(){
		$.ajax({
			type: "POST",
			url: "data/ledige_placeringer.php?ugenr=" + $(".rediger_online_ugenr").val(),
			data: 'hej',
			async: true,
			dataType: "html",
			success: function(msg){
				$(".placeringsselect").html(msg);
				$(".placeringsrakke").css("display", "table-row");
				}
	
			});
	});
}   
	

function onlineSkiftUge(){
	$("#frem a").click(function(){
		$("#frem").submit();
	})
	
	$("#tilbage a").click(function(){
		$("#tilbage").submit();
	})
}


function genfaktureringselect() {
	$(".genfaktureringselect").change(function(){
		$(this).parent().submit();
	});
}

function brancheguideandring(){
	$(".brancheguideandret").change(function(){
		if($(this).val() == 0){
			$(".brancheguideandring").css("display", "table-row");
		}
		else {
			$(".brancheguideandring").css("display", "none");
		}
	});
}
		
$(document).ready(function(){
	scrolling()	
	skiftGraf()
	vistype()
	hentMagasin()
	hentArType()
	loaddata()
	ledigeOnline()
	onlineSkiftUge()
	redigerOnline()
	genfaktureringselect()
	brancheguideandring()
});



