<?php include("../../database.php"); 

$id = $_GET["id"];
	
	if($id == 17){
		header("Location: http://estatemedia.dk/konference.html");
	}
	
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Projektsalg", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder", "Energioptimering", "Portaler for salg og udlejning", "Forsikringsmæglere", "Parkeringsløsninger");

	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js.js"></script>

<link rel="stylesheet" type="text/css" href="estatemedia/style.css">

<title>Brancheguide - Estate Media</title>
</head>
<body>
<div class="page <?php echo strtolower(str_replace(",", "", str_replace(" ", "_", $brancheguidekat[$id-1]))); ?>">
	<div class="top"></div>		
	<table class="brancheguidetable">
	<?php
		$kategorien = "";
	    $resultat = mysql_query("SELECT * FROM salg_brancheguide ORDER BY kategori, navn");
	    if(!$resultat){
	    	  die('Could not connect: ' . mysql_error());
	    	  }
	    	  	  
	    while($brancheguide = mysql_fetch_array($resultat)){

	    	$brancheguideid = $brancheguide["id"];
	    				
	    				if($kategorien != $brancheguide["kategori"]){
		    				?>
		    				<tr>
			    				<td colspan="2"><h1 style="padding-top: 25px;"><?php echo $brancheguidekat[$brancheguide["kategori"] - 1]; ?></h1></td>
		    				</tr>
		    				<?php
	    				}
	    				
						?>
						<tr>
							<td class="billede"><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><img src="upload/<?php echo $brancheguide["logourl"]; ?>" /></a></td>
							<td class="tekst">
								
								<h1><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><?php echo $brancheguide["navn"]; ?></a></h1>
								<p><?php echo stripslashes($brancheguide["adresse"]); ?>, tlf.: <?php echo $brancheguide["telefon"]; ?></p>
								<p><?php if($brancheguide["kontaktperson"] != ""){?>Kontakt: <?php echo $brancheguide["kontaktperson"]; ?>, <?php } ?>E-mail: <a href="mailto:<?php echo $brancheguide["email"]; ?>"> <?php echo $brancheguide["email"]; ?></a></p>
								<p><?php echo nl2br($brancheguide["tekst"]); ?></p>
								
							</td>
						
						</tr>
						<?php
						$kategorien = $brancheguide["kategori"];
	    	}
	    	?>
	</div>


</body>
</html>