<?php 
session_start();
$admin = $_SESSION["admin"];

if (!isset($_SESSION["brugernavn"]) || !isset($_SESSION["brugerid"]) || !isset($_SESSION["salg"])) {
   	session_destroy();
    header("Location: ../index.php");
}  
?>