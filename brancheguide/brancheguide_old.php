<?php include("login_kontrol.php"); include("../database.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
<?php
	$nr = $_GET["nr"];
?>
<div class="annoncer brancheguides">
	<?php
	if(isset($_GET["periode"])){
		if($_GET["periode"] == "alle"){?>
			<h1 class="exceldownload"><a href="excel.php?periode=alle">Download</a></h1>
			<?php
			}
		else {
				?>
				<h1 class="exceldownload"><a href="excel.php?periode=<?php echo $_GET["periode"]; ?>">Download</a></h1>
				<?php
			}
		}
	else {
		?>
		<h1 class="exceldownload"><a href="excel.php">Download</a></h1>
		<?php
		}
			?>
	<h1 class="menulink"><a href="../forside.php">Menu</a></h1>
	
	<form method="get" action="brancheguide.php">
		<select class="magasinselect" name="periode">
		    <option <?php if($_GET["periode"] == "alle"){echo "selected";} ?> value="alle">Alle</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-01-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-01-15">Januar</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-02-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-02-15">Februar</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-03-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-03-15">Marts</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-04-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-04-15">April</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-05-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-05-15">Maj</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-06-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-06-15">Juni</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-07-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-07-15">Juli</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-08-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-08-15">August</option>
		    <option <?php if($_GET["periode"] == date("Y") . "-09-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-09-15">September</option>
			<option <?php if($_GET["periode"] == date("Y") . "-10-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-10-15">Oktober</option>
			<option <?php if($_GET["periode"] == date("Y") . "-11-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-11-15">November</option>
			<option <?php if($_GET["periode"] == date("Y") . "-12-15"){echo "selected";} ?> value="<?php echo date("Y"); ?>-12-15">December</option> 
		</select>
	</form>
	<p class="headbeskrivelse">Startmåned: </p>
	
	
	
	
	
	
	<?php
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Projektsalg", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder", "Energioptimering", "Portaler for salg og udlejning", "Forsikringsmæglere", "Parkeringsløsninger");
		for($i = 1; $i < (count($brancheguidekat) + 1); $i++){
		
		if(isset($_GET["periode"])){
			if($_GET["periode"] == "alle"){
				$resultatet = mysql_query("SELECT * FROM salg_handler WHERE type = '5' AND brancheguidekat = '$i' ORDER BY brancheguidekat");
				$kategoriadskiller = 'brancheguidekat';
			}
			else {
				$maned = $_GET["periode"];
				$resultatet = mysql_query("SELECT * FROM salg_handler WHERE type = '5' AND brancheguidekat = '$i' AND maned = '$maned' ORDER BY brancheguidekat");
			}
			}
		else {
			$resultatet = mysql_query("SELECT * FROM salg_handler WHERE type = '5' AND brancheguidekat = '$i' ORDER BY brancheguidekat");	
			}

		if(!$resultatet){
			die('Could not connect: ' . mysql_error());
			}
		
		if(mysql_num_rows($resultatet) > 0){
		    ?>
			 <h1><?php echo $brancheguidekat[$i-1]; ?></h1>
			    <table>
			    	<tr>
			    		<th></th>
			    		<th>Kunde</th>
			    		<th>Note</th>
			    		<th></th>
			    		<th></th>
			    	</tr>
			    <?php
			    while($annonce = mysql_fetch_array($resultatet)){
			    
			   
				    
			    $brancheguideid = $annonce["brancheguideid"];
			    $resultat = mysql_query("SELECT * FROM salg_brancheguide WHERE id = '$brancheguideid'");
			    $brancheguiden = mysql_fetch_array($resultat);
			    
			    
			    
			    ?>
			    	<tr>
			    		<td><img src="upload/<?php echo $brancheguiden["logourl"]; ?>" /></td>
						<td><p><?php echo $brancheguiden["navn"];?></p></td>
						<td><p><?php echo nl2br($annonce["note"]); ?></p></td>
						<td><p></p></td>
						<td><p><a href="rediger.php?id=<?php echo $brancheguiden["id"]; ?>&oversigt=1">Rediger</a></p></td>
			    	</tr>
			    
				<?php
		    	}
		    
		}
		?>
		</table>
		<?php
	}
	?>
		
	
			
</div>





</body>
</html>