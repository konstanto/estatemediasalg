<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/Copenhagen');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Estate Media")
							 ->setLastModifiedBy("Estate Media")
							 ->setTitle("Brancheguide")
							 ->setSubject("Brancheguide")
							 ->setDescription("Brancheguide")
							 ->setKeywords("")
							 ->setCategory("Brancheguide");

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A1", "Kunde")
            ->setCellValue("B1", "Adresse")
            ->setCellValue("C1", "Telefon")
            ->setCellValue("D1", "Kontaktperson")
            ->setCellValue("E1", "Email")
            ->setCellValue("F1", "Tekst")
            ->setCellValue("G1", "Kategori")
            ->setCellValue("H1", "Link")
            ->setCellValue("I1", "Note");
          
include("../database.php");

$today = date("Y-m-d H:i:s");

if(isset($_GET["periode"])){
	if($_GET["periode"] == "alle"){
		$resultat = mysql_query("SELECT * FROM salg_handler WHERE type = '5' ORDER BY brancheguidekat AND brancheguidestart < '$today'");
	}
	else {
		$maned = $_GET["periode"];
		$resultat = mysql_query("SELECT * FROM salg_handler WHERE type = '5' AND maned = '$maned' ORDER BY brancheguidekat");
	}
	}
else {
	$resultat = mysql_query("SELECT * FROM salg_handler WHERE type = '5' ORDER BY brancheguidekat AND brancheguidestart < '$today'");	
	}

$linjenummer = 2;
while($ordre = mysql_fetch_array($resultat)){
	
	$brancheguiden = $ordre["brancheguideid"];
	$resultatet = mysql_query("SELECT * FROM salg_brancheguide WHERE id = '$brancheguiden'");
	$brancheguide = mysql_fetch_array($resultatet);
	
	
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Projektsalg", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder"); 
	
	$kategori = $brancheguidekat[$brancheguide["kategori"] - 1];
	
	
	
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$linjenummer", "$brancheguide[navn]")
            ->setCellValue("B$linjenummer", "$brancheguide[adresse]")
            ->setCellValue("C$linjenummer", "$brancheguide[telefon]")
            ->setCellValue("D$linjenummer", "$brancheguide[kontaktperson]")
            ->setCellValue("E$linjenummer", "$brancheguide[email]")
            ->setCellValue("F$linjenummer", "$brancheguide[tekst]")
            ->setCellValue("G$linjenummer", "$kategori")
            ->setCellValue("H$linjenummer", "$brancheguide[link]")
            ->setCellValue("I$linjenummer", "$ordre[note]");

    $linjenummer++;        
	}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Brancheguide');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Brancheguide.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
