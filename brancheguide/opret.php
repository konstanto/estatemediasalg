<?php include("login_kontrol.php"); include("../database.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=8"></meta> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder brancheguide_opret">
		<div class="opretboks">
			<div class="header">
				<a href="index.php" class="menu"><h1>Menu</h1></a>
				<h1>Opret ny brancheguide</h1>
			</div>
			<form method="post" action="opret_send.php" enctype="multipart/form-data">
				<table>
					<tr>
						<td><p>Firmanavn:</p></td>
						<td><p>Engelsk titel:</p></td>
					</tr>
					<tr>
						<td><input type="text" name="navn" /></td>
						<td><input type="text" name="navn_eng" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Adresse:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="adresse" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Telefon:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="telefon" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Kontaktperson:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="kontakt" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Email:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="email" /></td>
					</tr>
					<tr>
						<td><p>Tekst:</p></td>
						<td><p>Engelsk:</p></td>
					</tr>
					<tr>
						<td><textarea name="tekst"></textarea></td>
						<td><textarea name="tekst_eng"></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><p>Tekst sidst rettet:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="text_last_edited" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Link:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="link" /></td>
					</tr>
					<tr>
						<td><p>Pris:</p></td>
						<td><p>Fakturanr:</p></td>
					</tr>
					<tr>
						<td><input type="text" name="pris" /></td>
						<td><input type="text" name="fakturanr" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Genfakturering</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="genfakturering" style="width: 101%;">
								<option value="1">Januar</option>
								<option value="2">Februar</option>
								<option value="3">Marts</option>
								<option value="4">April</option>
								<option value="5">Maj</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><p>Kategori</p></td>
						<td><p>Logo</p></td>
					</tr>
					<tr>
						<td>
							<select name="kategori" style="width: 101%;">
								<option value="1">Administratorer</option>
								<option value="2">Advokater</option>
								<option value="3">Arkitekter</option>
								<option value="4">Asset Management</option>
								<option value="5">Bygherrerådgivere</option>
								<option value="6">Ejendomsselskaber</option>
								<option value="25">Energioptimering</option>
								<option value="7">Entreprenører</option>
								<option value="8">Erhvervsejendomsmæglere</option>
								<option value="9">Facility management udbydere</option>
								<option value="10">Finansiel rådgivning</option>
								<option value="11">Finansieringsselskaber</option>
								<option value="12">Foreninger</option>
								<option value="27">Forsikringsmægler</option>
								<option value="13">Forsikringsselskaber</option>
								<option value="14">Indretning</option>
								<option value="15">Ingeniører</option>
								<option value="16">Investeringsselskaber</option>
								<option value="28">Parkeringsløsninger</option>
								<option value="17">Projektsalg</option>
								<option value="18">Landinspektører</option>
								<option value="19">Medie, reklame, og kommunikation</option>
								<option value="26">Portaler for salg og udlejning</option>
								<option value="20">Projektudviklere</option>
								<option value="21">Rekruttering</option>
								<option value="22">Retail Management</option>
								<option value="23">Revisorer</option>
								<option value="24">Sikkerhedsvirksomheder</option>
							</select>
						</td>
						<td><input type="file" name="logo" /></td>
					</tr>
					
					<tr>
						<td colspan="2"><p>Noter:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="brancheguidenoter"></textarea></td>
					</tr>
					
					
					
					
					
					<tr>
						<td colspan="2"><input type="submit" value="Opret brancheguide" />
					</tr>
			
			
				</table>
			</form>
		</div>
	
	</div>


</body>
</html>