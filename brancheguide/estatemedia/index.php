<?php include("../../database.php"); 
			
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Konferencer og efteruddannelse", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder", "Energioptimering", "Portaler for salg og udlejning", "Forsikringsmægler");
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js.js"></script>

<link rel="stylesheet" type="text/css" href="style.css">

<title>Brancheguide - Estate Media</title>
</head>
<body>
	<div class="page">
		<div class="top"></div>
		<a target="_parent" class="orangebox" href="http://estatemedia.dk/brancheguide/optagelse-i-guiden.html"></a>
		<div class="fronttext">
			<h1>Find virksomheder beskæftiget i branchen</h1>
			<p>Velkommen til Estate Medias brancheguide. Her finder du et praktisk og hurtigt overblik over en række af de virksomheder og personer, der arbejder med byggeri og ejendom i Danmark.</p>
		</div>
		<select name="kategori" class="kategoriselect">
			<option value="false">Find Branche</option>
				<option value="1">Administratorer</option>
				<option value="2">Advokater</option>
				<option value="3">Arkitekter</option>
				<option value="4">Asset Management</option>
				<option value="5">Bygherrerådgivere</option>
				<option value="6">Ejendomsselskaber</option>
				<option value="25">Energioptimering</option>
				<option value="7">Entreprenører</option>
				<option value="8">Erhvervsejendomsmæglere</option>
				<option value="9">Facility management udbydere</option>
				<option value="10">Finansiel rådgivning</option>
				<option value="11">Finansieringsselskaber</option>
				<option value="12">Foreninger</option>
				<option value="27">Forsikringsmæglere</option>
				<option value="13">Forsikringsselskaber</option>
				<option value="14">Indretning</option>
				<option value="15">Ingeniører</option>
				<option value="16">Investeringsselskaber</option>
				<option value="17">Projektsalg</option>
				<option value="18">Landinspektører</option>
				<option value="19">Medie, reklame, og kommunikation</option>
				<option value="26">Portaler for salg og udlejning</option>
				<option value="20">Projektudviklere</option>
				<option value="21">Rekruttering</option>
				<option value="22">Retail Management</option>
				<option value="23">Revisorer</option>
				<option value="24">Sikkerhedsvirksomheder</option>
		</select>
		<div class="brancheguideplakat"></div>
		
		<h1 class="brancheguide_header">Brancheguide</h1>
		<div class="kategorier">
			<a href="kategori.php?id=1"><div class="kategori administratorer"></div></a>
			<a href="kategori.php?id=2"><div class="kategori advokater"></div></a>
			<a href="kategori.php?id=3"><div class="kategori arkitekter"></div></a>
			<a href="kategori.php?id=4"><div class="kategori asset_management"></div></a>
			<a href="kategori.php?id=5"><div class="kategori bygherrerådgivere"></div></a>
			<a href="kategori.php?id=6"><div class="kategori ejendomsselskaber"></div></a>
			<a href="kategori.php?id=25"><div class="kategori energioptimering"></div></a>
			<a href="kategori.php?id=7"><div class="kategori entreprenører"></div></a>
			<a href="kategori.php?id=8"><div class="kategori erhvervsejendomsmæglere"></div></a>
			<a href="kategori.php?id=9"><div class="kategori facility_management_udbydere"></div></a>
			<a href="kategori.php?id=10"><div class="kategori finansiel_rådgivning"></div></a>
			<a href="kategori.php?id=11"><div class="kategori finansieringsselskaber"></div></a>
			<a href="kategori.php?id=12"><div class="kategori foreninger"></div></a>
			<a href="kategori.php?id=27"><div class="kategori forsikringsmagler"></div></a>
			<a href="kategori.php?id=13"><div class="kategori forsikringsselskaber"></div></a>
			<a href="kategori.php?id=14"><div class="kategori indretning"></div></a>
			<a href="kategori.php?id=15"><div class="kategori ingeniører"></div></a>
			<a href="kategori.php?id=16"><div class="kategori investeringsselskaber"></div></a>
			<a href="kategori.php?id=17"><div class="kategori konferencer_og_efteruddannelse"></div></a>
			<a href="kategori.php?id=18"><div class="kategori landinspektører"></div></a>
			<a href="kategori.php?id=19"><div class="kategori medie_reklame_og_kommunikation"></div></a>
			<a href="kategori.php?id=26"><div class="kategori portaler_for_salg_og_udlejning"></div></a>
			<a href="kategori.php?id=20"><div class="kategori projektudviklere"></div></a>
			<a href="kategori.php?id=21"><div class="kategori rekruttering"></div></a>
			<a href="kategori.php?id=22"><div class="kategori retail_management"></div></a>
			<a href="kategori.php?id=23"><div class="kategori revisorer"></div></a>
			<a href="kategori.php?id=24"><div class="kategori sikkerhedsvirksomheder"></div></a>
			<?php
			
			/*
for($i = 1; $i < count($brancheguidekat) + 1; $i++){
				?>
				<a href="kategori.php?id=<?php echo $i; ?>"><div class="kategori <?php echo strtolower(str_replace(",", "", str_replace(" ", "_", $brancheguidekat[$i-1]))); ?>"></div></a>
				<?php
			}
*/
			?>
				
		</div>
		
		<div class="search">
			<h1>Søg i Brancheguiden</h1>
			<form action="search.php" method="get">
				<input class="query" type="text" name="query" />
				<input class="submit" type="submit" value="Søg" />
			</form>
		</div>
	</div>


</body>
</html>