<?php include("../../database.php"); 

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="jssor.js"></script>
<script type="text/javascript" src="jssor.slider.mini.js"></script>

<script type="text/javascript">
jQuery(document).ready(function ($) {
            var options = {
				$AutoPlayInterval: 1250,
                $AutoPlay: true,                                   //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $DragOrientation: 0                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options); 
            });
</script>


<style type="text/style">

.innerContainer {
	position: relative;
	height: 100%;
	width: 100%;
	-webkit-transform: perspective(0px) !important;
}

</style>

<title>Brancheguide - Estate Media</title>
</head>
<body>
	<div style="width: 268px; height: 268px; position: relative; box-sizing: border-box; padding-top: 84px;">
	<div style="width: 268px; height: 268px; position: relative; display: block; margin: 0 auto;">
<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 268px;
        height: 268px;">
        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 268px; height: 268px;
            overflow: hidden;">
            <?php 
	        $resultat = mysql_query("SELECT * FROM salg_brancheguide ORDER BY navn");
	        while($brancheguide = mysql_fetch_array($resultat)){
		        
		        ?>
				<div>
					<div class="innerContainer">
						<img u="image" src="../upload/<?php echo $brancheguide["logourl"]; ?>" style="max-width: 100%; max-height: 100%; display: block;	margin: 0px auto;" />
					</div>
					</div>
        <?php } ?>
            
                    </div>
    </div>
	</div>
	</div>
	</body>
</html>