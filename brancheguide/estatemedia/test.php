<?php include("../../database.php"); 

$id = $_GET["id"];
	
	if($id == 17){
		header("Location: http://estatemedia.dk/konference.html");
	}
	
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Projektsalg", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder");

	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js.js"></script>

<link rel="stylesheet" type="text/css" href="style.css">

<title>Brancheguide - Estate Media</title>
</head>
<body>
<div class="page <?php echo strtolower(str_replace(",", "", str_replace(" ", "_", $brancheguidekat[$id-1]))); ?>">
	<div class="top"></div>		
	<select name="kategori" class="kategoriselect">
	    <option value="false">Find Branche</option>
	    <?php
	    
	    for($i = 1; $i < count($brancheguidekat) + 1; $i++){
	    	?>
	    	<option value="<?php echo $i; ?>"><?php echo $brancheguidekat[$i-1]; ?></option>
	    	<?php
	    }
	    ?>
	</select>
	<table class="brancheguidetable">
	<?php
	    $resultat = mysql_query("SELECT * FROM salg_brancheguide");
	    if(!$resultat){
	    	  die('Could not connect: ' . mysql_error());
	    	  }
	    	  	  
	    while($brancheguide = mysql_fetch_array($resultat)){
	    	$brancheguideid = $brancheguide["id"];
	    	$antal_resultat = mysql_query("SELECT * FROM salg_handler WHERE brancheguideid = '$brancheguideid'");
	    	if(mysql_num_rows($antal_resultat) > 0){
	    		$handel = mysql_fetch_array($antal_resultat);
	    		$today = strtotime(date("Y-m-d"));
	    		
		    	if($brancheguide["logourl"] != "" && $brancheguide["navn"] != "" && $brancheguide["adresse"] != "" && $brancheguide["telefon"] != "" && $brancheguide["kontaktperson"] != "" && $brancheguide["email"] != "" && $brancheguide["tekst"] != "" && $brancheguide["link"] != "" && strtotime($handel["brancheguidestart"]) < $today){
				
						?>
						<tr>
							<td class="billede"><img src="../upload/<?php echo $brancheguide["logourl"]; ?>" /></td>
							<td class="tekst">
								
								<h1><a href="<?php echo $brancheguide["link"]; ?>" target="_blank"><?php echo $brancheguide["navn"]; ?></a></h1>
								<p><?php echo $brancheguide["adresse"]; ?>, tlf.: <?php echo $brancheguide["telefon"]; ?></p>
								<p>Kontakt: <?php echo $brancheguide["kontaktperson"]; ?>, E-mail: <a href="mailto:<?php echo $brancheguide["email"]; ?>"> <?php echo $brancheguide["email"]; ?></a></p>
								<p><?php echo nl2br($brancheguide["tekst"]); ?></p>
								
							</td>
						
						</tr>
						<?php
						}
					}	
	    	}
	    	?>
	</div>


</body>
</html>