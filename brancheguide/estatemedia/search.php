<?php include("../../database.php"); 

$query = mysql_real_escape_string($_GET["query"]);

if($id == 17){
	header("Location: http://estatemedia.dk/konference");
}
	
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Projektsalg", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder", "Forsikringsmægler");

	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js.js"></script>

<link rel="stylesheet" type="text/css" href="style.css">

<title>Brancheguide - Estate Media</title>
</head>
<body>
	<div class="page searchpage">
		<div class="top"></div>		
		<div class="search">
			<h1>Søg i Brancheguiden</h1>
			<form action="search.php" method="get">
				<input class="query" type="text" name="query" />
				<input class="submit" type="submit" value="Søg" />
			</form>
		</div>
		<table class="brancheguidetable">
			<?php 
			
			mysql_query("ALTER TABLE salg_brancheguide ADD FULLTEXT(navn, adresse, telefon, kontaktperson, email, tekst)");
			$resultat = mysql_query("SELECT * FROM salg_brancheguide WHERE MATCH (navn, adresse, telefon, kontaktperson, email, tekst) AGAINST ('$query' IN NATURAL LANGUAGE MODE)");
			if(!$resultat){
				  die('Could not connect: ' . mysql_error());
				  }
			
			while($brancheguide = mysql_fetch_array($resultat)){
				$brancheguideid = $brancheguide["id"];

							?>
					
					<tr>
						<td class="billede"><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><img src="../upload/<?php echo $brancheguide["logourl"]; ?>" /></a></td>
						<td class="tekst">
							
							<h1><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><?php echo $brancheguide["navn"]; ?></a></h1>
							<p><?php echo $brancheguide["adresse"]; ?>, tlf.: <?php echo $brancheguide["telefon"]; ?></p>
							<p><?php if($brancheguide["kontaktperson"] != ""){?>Kontakt: <?php echo $brancheguide["kontaktperson"]; ?>, <?php } ?>E-mail: <a href="mailto:<?php echo $brancheguide["email"]; ?>"> <?php echo $brancheguide["email"]; ?></a></p>
							<p><?php echo nl2br($brancheguide["tekst"]); ?></p>
							
						</td>
					
					</tr>
					<?php
				}
			?>
		</table>
	</div>


</body>
</html>