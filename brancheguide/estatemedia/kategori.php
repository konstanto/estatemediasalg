<?php include("../../database.php"); 

$id = $_GET["id"];
	
	if($id == 17){
		header("Location: http://estatemedia.dk/konference.html");
	}
	
	$brancheguidekat = array("Administratorer", "Advokater", "Arkitekter", "Asset Management", "Bygherrerådgivere", "Ejendomsselskaber", "Entreprenører", "Erhvervsejendomsmæglere", "Facility management udbydere", "Finansiel rådgivning", "Finansieringsselskaber", "Foreninger", "Forsikringsselskaber", "Indretning", "Ingeniører", "Investeringsselskaber", "Konferencer og efteruddannelse", "Landinspektører", "Medie, reklame, og kommunikation", "Projektudviklere", "Rekruttering", "Retail Management", "Revisorer", "Sikkerhedsvirksomheder", "Energioptimering", "Portaler for salg og udlejning", "Forsikringsmægler");

	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="../../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="js.js"></script>

<link rel="stylesheet" type="text/css" href="style.css">

<title>Brancheguide - Estate Media</title>
</head>
<body>
<div class="page <?php echo strtolower(str_replace(",", "", str_replace(" ", "_", $brancheguidekat[$id-1]))); ?>">
	<div class="top"></div>		
	<select name="kategori" class="kategoriselect">
	    <option value="false">Find Branche</option>
				<option value="1">Administratorer</option>
				<option value="2">Advokater</option>
				<option value="3">Arkitekter</option>
				<option value="4">Asset Management</option>
				<option value="5">Bygherrerådgivere</option>
				<option value="6">Ejendomsselskaber</option>
				<option value="25">Energioptimering</option>
				<option value="7">Entreprenører</option>
				<option value="8">Erhvervsejendomsmæglere</option>
				<option value="9">Facility management udbydere</option>
				<option value="10">Finansiel rådgivning</option>
				<option value="11">Finansieringsselskaber</option>
				<option value="12">Foreninger</option>
				<option value="27">Forsikringsmæglere</option>
				<option value="13">Forsikringsselskaber</option>
				<option value="14">Indretning</option>
				<option value="15">Ingeniører</option>
				<option value="16">Investeringsselskaber</option>
				<option value="17">Projektsalg</option>
				<option value="18">Landinspektører</option>
				<option value="19">Medie, reklame, og kommunikation</option>
				<option value="26">Portaler for salg og udlejning</option>
				<option value="20">Projektudviklere</option>
				<option value="21">Rekruttering</option>
				<option value="22">Retail Management</option>
				<option value="23">Revisorer</option>
				<option value="24">Sikkerhedsvirksomheder</option>
	</select>
	<table class="brancheguidetable">
	<?php
	    $resultat = mysql_query("SELECT * FROM salg_brancheguide WHERE kategori = '$id' ORDER BY navn");
	    if(!$resultat){
	    	  die('Could not connect: ' . mysql_error());
	    	  }
	    	  	  
	    while($brancheguide = mysql_fetch_array($resultat)){

	    	$brancheguideid = $brancheguide["id"];
	    		
						?>
						<tr>
							<td class="billede"><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><img src="../upload/<?php echo $brancheguide["logourl"]; ?>" /></a></td>
							<td class="tekst">
								
								<h1><a href="http://<?php echo $brancheguide["link"]; ?>" target="_blank"><?php echo $brancheguide["navn"]; ?></a></h1>
								<p><?php echo stripslashes($brancheguide["adresse"]); ?>, tlf.: <?php echo $brancheguide["telefon"]; ?></p>
								<p><?php if($brancheguide["kontaktperson"] != ""){?>Kontakt: <?php echo $brancheguide["kontaktperson"]; ?>, <?php } ?>E-mail: <a href="mailto:<?php echo $brancheguide["email"]; ?>"> <?php echo $brancheguide["email"]; ?></a></p>
								<p><?php echo nl2br($brancheguide["tekst"]); ?></p>
								
							</td>
						
						</tr>
						<?php
	    	}
	    	?>
	</div>


</body>
</html>