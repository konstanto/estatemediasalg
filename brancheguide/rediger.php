<?php include("login_kontrol.php"); include("../database.php"); 
	
	$id = $_GET["id"];
	$resultat = mysql_query("SELECT * FROM salg_brancheguide WHERE id = '$id'");
	if(!$resultat){
		  die('Could not connect: ' . mysql_error());
		  }
	
	$brancheguide = mysql_fetch_array($resultat);
	
	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=8"></meta> 
<script type="text/javascript" src="../js/jquery.js"></script>
<!--[if lt IE 12]> 
<link rel="stylesheet" type="text/css" href="../js/html5/ie.css" />
<![endif]-->
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../js/js.js"></script>

<link rel="stylesheet" type="text/css" href="../css/style.css">

<title>Salg - Estate Media</title>
</head>
<body>
	<div class="frontpage neworder brancheguide_opret">
		<div class="opretboks">
			<div class="header">
				<?php
				if($_GET["oversigt"] == 1){
					?>
					<a href="brancheguide.php" class="menu"><h1>Tilbage</h1></a>
					<?php
					}
				else {
					?>
					<a href="index.php" class="menu"><h1>Menu</h1></a>
					<?php
				}
				?>
				<h1>Rediger brancheguide</h1>
			</div>
			<form method="post" action="rediger_send.php" enctype="multipart/form-data">
				<input type="hidden" value="<?php echo $_GET["oversigt"]; ?>" name="oversigt" />
				<input type="hidden" value="<?php echo $id; ?>" name="id" />
				<table>
					<tr>
						<td><p>Firmanavn:</p></td>
						<td><p>Engelsk titel:</p></td>
					</tr>
					<tr>
						<td><input type="text" value="<?php echo $brancheguide["navn"]; ?>" name="navn" /></td>
						<td><input type="text" value="<?php echo $brancheguide["titel_eng"]; ?>" name="navn_eng" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Link:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["link"]; ?>" name="link" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Adresse:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["adresse"]; ?>" name="adresse" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Telefon:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["telefon"]; ?>" name="telefon" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Kontaktperson:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["kontaktperson"]; ?>" name="kontakt" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Email:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["email"]; ?>" name="email" /></td>
					</tr>
					<tr>
						<td><p>Tekst:</p></td>
						<td><p>Engelsk:</p></td>
					</tr>
					<tr>
						<td><textarea name="tekst"><?php echo $brancheguide["tekst"]; ?></textarea></td>
						<td><textarea name="beskrivelse_eng"><?php echo $brancheguide["beskrivelse_eng"]; ?></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><p>Tekst sidst rettet:</p></td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" value="<?php echo $brancheguide["text_last_edited"]; ?>" name="text_last_edited" /></td>
					</tr>
					<tr>
						<td><p>Pris:</p></td>
						<td><p>Fakturanr:</p></td>
					</tr>
					<tr>
						<td><input type="text" value="<?php echo $brancheguide["pris"]; ?>" name="pris" /></td>
						<td><input type="text" value="<?php echo $brancheguide["fakturanr"]; ?>" name="fakturanr" /></td>
					</tr>
					<tr>
						<td colspan="2"><p>Genfakturering</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="genfakturering" style="width: 101%;">
								<option <?php if($brancheguide["genfakturering_date"] == 1){echo "selected";} ?> value="1">Januar</option>
								<option <?php if($brancheguide["genfakturering_date"] == 2){echo "selected";} ?> value="2">Februar</option>
								<option <?php if($brancheguide["genfakturering_date"] == 3){echo "selected";} ?> value="3">Marts</option>
								<option <?php if($brancheguide["genfakturering_date"] == 4){echo "selected";} ?> value="4">April</option>
								<option <?php if($brancheguide["genfakturering_date"] == 5){echo "selected";} ?> value="5">Maj</option>
								<option <?php if($brancheguide["genfakturering_date"] == 6){echo "selected";} ?> value="6">Juni</option>
								<option <?php if($brancheguide["genfakturering_date"] == 7){echo "selected";} ?> value="7">Juli</option>
								<option <?php if($brancheguide["genfakturering_date"] == 8){echo "selected";} ?> value="8">August</option>
								<option <?php if($brancheguide["genfakturering_date"] == 9){echo "selected";} ?> value="9">September</option>
								<option <?php if($brancheguide["genfakturering_date"] == 10){echo "selected";} ?> value="10">Oktober</option>
								<option <?php if($brancheguide["genfakturering_date"] == 11){echo "selected";} ?> value="11">November</option>
								<option <?php if($brancheguide["genfakturering_date"] == 12){echo "selected";} ?> value="12">December</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><p>Kategori</p></td>
						<td><p>Logo</p></td>
					</tr>
					<tr>
						<td>
							<select name="kategori" style="width: 101%;">
								<option <?php if($brancheguide["kategori"] == 1){echo "selected";} ?> value="1">Administratorer</option>
								<option <?php if($brancheguide["kategori"] == 2){echo "selected";} ?> value="2">Advokater</option>
								<option <?php if($brancheguide["kategori"] == 3){echo "selected";} ?> value="3">Arkitekter</option>
								<option <?php if($brancheguide["kategori"] == 4){echo "selected";} ?> value="4">Asset Management</option>
								<option <?php if($brancheguide["kategori"] == 5){echo "selected";} ?> value="5">Bygherrerådgivere</option>
								<option <?php if($brancheguide["kategori"] == 6){echo "selected";} ?> value="6">Ejendomsselskaber</option>
								<option <?php if($brancheguide["kategori"] == 25){echo "selected";} ?> value="25">Energioptimering</option>
								<option <?php if($brancheguide["kategori"] == 7){echo "selected";} ?> value="7">Entreprenører</option>
								<option <?php if($brancheguide["kategori"] == 8){echo "selected";} ?> value="8">Erhvervsejendomsmæglere</option>
								<option <?php if($brancheguide["kategori"] == 9){echo "selected";} ?> value="9">Facility management udbydere</option>
								<option <?php if($brancheguide["kategori"] == 10){echo "selected";} ?> value="10">Finansiel rådgivning</option>
								<option <?php if($brancheguide["kategori"] == 11){echo "selected";} ?> value="11">Finansieringsselskaber</option>
								<option <?php if($brancheguide["kategori"] == 12){echo "selected";} ?> value="12">Foreninger</option>
								<option <?php if($brancheguide["kategori"] == 27){echo "selected";} ?> value="27">Forsikringsmæglere</option>
								<option <?php if($brancheguide["kategori"] == 13){echo "selected";} ?> value="13">Forsikringsselskaber</option>
								<option <?php if($brancheguide["kategori"] == 14){echo "selected";} ?> value="14">Indretning</option>
								<option <?php if($brancheguide["kategori"] == 15){echo "selected";} ?> value="15">Ingeniører</option>
								<option <?php if($brancheguide["kategori"] == 16){echo "selected";} ?> value="16">Investeringsselskaber</option>
								<option <?php if($brancheguide["kategori"] == 17){echo "selected";} ?> value="17">Konferencer og efteruddannelse</option>
								<option <?php if($brancheguide["kategori"] == 18){echo "selected";} ?> value="18">Landinspektører</option>
								<option <?php if($brancheguide["kategori"] == 19){echo "selected";} ?> value="19">Medie, reklame, og kommunikation</option>
								<option <?php if($brancheguide["kategori"] == 28){echo "selected";} ?> value="28">Parkeringsløsninger</option>
								<option <?php if($brancheguide["kategori"] == 20){echo "selected";} ?> value="20">Projektudviklere</option>
								<option <?php if($brancheguide["kategori"] == 21){echo "selected";} ?> value="21">Rekruttering</option>
								<option <?php if($brancheguide["kategori"] == 22){echo "selected";} ?> value="22">Retail Management</option>
								<option <?php if($brancheguide["kategori"] == 23){echo "selected";} ?> value="23">Revisorer</option>
								<option <?php if($brancheguide["kategori"] == 26){echo "selected";} ?> value="26">Portaler for salg og udlejning</option>
								<option <?php if($brancheguide["kategori"] == 24){echo "selected";} ?> value="24">Sikkerhedsvirksomheder</option>

							</select>
						</td>
						<td><input type="file" name="logo" /></td>
					</tr>
					
					<tr>
						<td colspan="2"><p>Ændring:</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="brancheguideandret" style="width: 101%;">
								<option <?php if($brancheguide["brancheguideandret"] == 1){echo "selected";} ?> value="1">Ændret</option>
								<option <?php if($brancheguide["brancheguideandret"] == 0){echo "selected";} ?> value="0">Ikke ændret</option>
							</select>
						</td>
					</tr>
					
					<tr>
						<td colspan="2"><p>Ændring:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="brancheguideandring"><?php echo $brancheguide["brancheguideandring"]; ?></textarea></td>
					</tr>
					
					<tr>
						<td colspan="2"><p>Noter:</p></td>
					</tr>
					<tr>
						<td colspan="2"><textarea name="brancheguidenoter"><?php echo $brancheguide["noter"]; ?></textarea></td>
					</tr>
					
					<!--<tr>
						<td colspan="2"><p>Tilknyttede ordrer:</p></td>
					</tr>
					<tr>
						<td colspan="2">
							<?php
								$today_month = date("Y-m") . "-15";
								$sql = mysql_query("SELECT * FROM salg_handler WHERE brancheguideid = '$id'");
								while($ordre = mysql_fetch_array($sql)){
									?>
									<p><a href="../data/rediger_ordre.php?id=<?php echo $ordre["id"]; ?>&maned=<?php echo $today_month; ?>">[<?php echo $ordre["id"]; ?>] <?php echo $ordre["kunde"]; ?></a></p>		
									<?php
								}
								?>
							</td>
					</tr>-->
					
					<tr>
						<td colspan="2"><input type="submit" value="Gem ændringer" />
					</tr>
			
			
				</table>
			</form>
		</div>
	
	</div>


</body>
</html>